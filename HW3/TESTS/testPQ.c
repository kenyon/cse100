#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "PQueue.h"

typedef struct BigInt { char space1[107]; int i; char space2[1005]; } * BigInt;
void * copyBigInt( BigInt to, BigInt from ) { memcpy( to, from, sizeof(struct BigInt) ); return to;}
void set( BigInt bi , int i) { bi->i = i; }

int testPQ()
{
  PQueue pq;
  BigInt bi;
  #define maxElements 1000
  int data[ maxElements ];
  int i,j,temp;
  srandom(0);

  pq = makePQueue( maxElements , sizeof (struct BigInt) , copyBigInt, free ) ;
  bi = (BigInt) malloc( sizeof(struct BigInt) );

  if ( fetchPQ( pq , bi ) == true ) {
    printf("Error: Your fetchPQ was empty, but fetchPQ returned true!\n");
    return -1;
  }

  /* first, fill up the data[] array so that data[i] == i */
  for( i = 0 ; i < maxElements ; i++) data[i] = i;

  /* second, shuffle the array */
  for( i = 0 ; i < maxElements-1 ; i++)
  {
    j = i+1+(random() % (maxElements-i-1));  /* pick a place to swap with */
    temp = data[i]; data[i] = data[j]; data[j] = temp; /* do the swap */
  }

  /* third, insert all of them into the PQueue */
  for( i = 0 ; i < maxElements ; i++)
  {
    printf("Inserting item #%d, (priority == data == %d)\r", i, data[i] );	// notice the \r not \n

    set(bi, data[i]);  /* Our "BigInt" has the value data[i] */
    if ( insertPQ( pq , bi , bi->i ) == false )
      printf("\nError: Cannot insert the %dth element (of value %d)!\n", i, data[i]);
    else
      continue;

    printf("[Stopping.]\n"); return -1;
  }

  printf("Done inserting items 0 through %d\t\t\t\t\t\t\t\t\n", maxElements-1);

  /* (Check that we can't insert into a full PQ) */
  if ( insertPQ( pq , bi , 1.0 ) == true ) {
    printf("\nError: Your fetchPQ is full and insertPQ returns true!\n");
    return -1;
  }


  /* forth, check that we can fetch ALL of them (and that they're in order) */
  for( i = maxElements-1 ; i >= 0 ; i-- ) /* go backwards, since we expect higher data to come first */
  {
    printf("Fetching item #%d ... (going backwards...)\r", i );		// notice the \r not \n here too.

    if ( fetchPQ( pq , bi ) == false )
      printf("\nError: Could not fetch the %dth element!\n", i );
    else if ( bi->i != i )
      printf("\nError: I expected item #%d to have value of %d (they should be sorted),"
	     "but instead I got %d\n", i , i , bi->i );
    else continue;

    printf("[Stopping.]\n"); return -1;
  }

  // printf("Done fetching items %d through 0\t\t\t\t\t\t\n", maxElements-1);
  printf("Done fetching items %d through 0\n", maxElements-1);

  /* before we finish, let's fill up the array and then call freePQueue to test that it doesn't crash */
  for( i = 0 ; i < maxElements ; i++)
    insertPQ( pq , bi, bi->i ); /* doesn't matter what we fill it with */

  freePQ ( &pq );

  if ( pq != NULL ) 
      printf("\nError: I expected to have a NULL value for pq here\n" ) ;
	
  free( bi );

  return 0;
}



int main(int argc, char** argv)
{
  if ( testPQ() == 0 )
    printf("No obvious errors in PQueue found!  Good job.\n");
  else
    printf("There was some errors in your PQueue; Try to fix them and run again.\n");

  return EXIT_SUCCESS;
}

