/** @file
 * Tester program for PQueuee.
 *
 * @author Kenyon Ralph
 */

#include <stdlib.h>
#include <stdio.h>

#include "Int.h"
#include "PQueue.h"

int main() {
    //while (1) { //memory leak testing

    PQueue pq = makePQueue(10, sizeofInt(), copyInt, freeInt);

    Int ints[10];

    unsigned int i;
    bool result = false;

    for (i = 0; i < 10; i++) {
	ints[i] = makeInt(random() % 1000);

	result = insertPQ(pq, ints[i], i);
	printf("ints[%d]=%d, insertPQ: %s.\n", i, valueInt(ints[i]), result ? "true" : "false");

	/*
	fetchPQ(pq, data);
	printf("data[%d]=%d.\n", i, valueInt(data));
	*/
    }

    printf("\n");

    Int data = makeInt(0);

    for (i = 0; i < 10; i++) {
	fetchPQ(pq, data);
	printf("data[%d]=%d.\n", i, valueInt(data));
    }

    for (i = 0; i < 10; i++) {
	freeInt(ints[i]);
    }

    freeInt(data);
    freePQ(&pq);

    //}

    return EXIT_SUCCESS;
}
