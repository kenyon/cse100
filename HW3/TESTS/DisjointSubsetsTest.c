/** @file
 * Test program for DisjointSubsets.c.
 *
 * @author Kenyon Ralph
 */

#include <stdlib.h>
#include <stdio.h>

#include "DisjointSubsets.h"

int main() {
    const unsigned int sizeDS = 10;

    //while (1) { //memory leak test

    DisjointSubsets ds = makeDisjointSubsets(sizeDS);

    //printf("ds=%p.\n", ds);

    printf("ds->count=%d.\n", get_DS_count(ds));

    unsigned int i;
    for (i = 0; i < get_DS_count(ds); i++) {
	printf("ds->parents[%d]=%d, heights=%d.\n", i, get_parent_at(ds, i), get_height_at(ds, i));
    }

    for (i = 0; i < get_DS_count(ds); i++) {
	printf("findDS(ds, %d)=%d, ", i, findDS(ds, i));
	printf("testDS(ds, %d)=%d.\n", i, testDS(ds, i));
    }

    freeDS(&ds);

    //printf("ds=%p.\n", ds);

    //} // end memory leak test

    return EXIT_SUCCESS;
}
