#include <stdio.h>

#include "PQueue.h"
#include "Int.h"

unsigned rNum [] = { 550782, 535176, 1104830, 1216057, 1405516, 1169576, 
		1172368, 83090, 2355115, 2366128, 379954, 644502, 468387, 890598,
 		1536880, 411926, 207412, 1456070, 2373041, 516716, 1920858, 1226442,
 		1956135, 2332621, 1085469, 1245600, 2044270, 2215429, 1818807 } ;



int
main ( )
{
	PQueue pq = makePQueue ( 34 , sizeofInt() , copyInt , freeInt ) ;
	Int ii ;
	int i ;

	ii = makeInt ( 1234 ) ;

	for ( i = 0 ; i < 6; i++ ) {
						   /* for ( i = 0 ; i < 25; i++ ) { */
		updateInt ( ii, i ) ;
		insertPQ ( pq , ii , (double)rNum[i] ) ;
	}

	while ( fetchPQ ( pq , ii ) == true ) 
		printf ( "%d\n" , valueInt(ii) ) ;
	freePQ ( & pq ) ;

	return EXIT_SUCCESS;
}
