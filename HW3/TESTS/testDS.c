#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "DisjointSubsets.h"


int TheTestDS()
{
  int i,j,n,temp;
  int root = -999;
  int rooti, rootj, even_root, odd_root;
  #define NUM_INSERTS 1000
  int data[NUM_INSERTS];

  DisjointSubsets ds;
  srandom(0);

  /* create data in order */
  for(i = 0 ; i < NUM_INSERTS ; i++) data[i] = i;

  /* shuffle the array */
  for(i = 0 ; i < NUM_INSERTS - 1 ; i++)
  {
    j = i+1+(random() % (NUM_INSERTS-i-1));  /* pick a place to swap with */
    temp = data[i]; data[i] = data[j]; data[j] = temp; /* do the swap */
  }


  printf("This program should be done in a couple seconds..."
	 "if not, you may have an error.\n"
	 "(Make sure path compression and link-by-height were implemented correctly.)\n\n");
  ds = makeDisjointSubsets ( NUM_INSERTS );
  printf("Done constructing the DisjointSubsets.\n");


  /* for each integer, make sure it is the root of itself
     (that is how it should be initialized by makeDS) */
  for(i = 0 ; i < NUM_INSERTS ; i++)
  {
    root = -1 ;
    root = findDS( ds , data[i] ) ;
    if ( root == -1 ) 
      printf("Error: Could not findDS(%d)!\n", data[i] );
    else if ( data[i] != root )
      printf("Error: Expecting %d to be the root of itself, but instead findDS set root=%d.\n", data[i] , root );
    else
      continue;

    return -1;
  }
  printf("Done checking that it was initialized properly.\n");

  
  /* We pick random pairs of numbers, and link them together if they are both odd or both even */
  for(n = 0 ; n < 10000 ; n++) /* doesn't really matter how many times we pick pairs */
  {
    i = random() % NUM_INSERTS;
    j = random() % NUM_INSERTS;
    if (i == j) continue; /* we want i != j */

    rooti = findDS( ds , data[i] );
    rootj = findDS( ds , data[j] );   
    if ( (data[i] % 2) == (data[j] % 2)  /* if they're both odd or both even */
	 && rooti != rootj )             /* and they're currently not in the same subset */
      unionDS( ds, rooti, rootj );        /* then link them together */
  }

  /* statistically, we probably don't need to do anything more; but let's make sure we covered all pairs */
  /* go through each pair, joining them together if they are both odd or both even */
  for(i = 0 ; i < NUM_INSERTS ; i++)
    for(j = i+1 ; j < NUM_INSERTS ; j++)
    {
      rooti = findDS( ds , data[i] );
      rootj = findDS( ds , data[j] );

      if ( (data[i] % 2) == (data[j] % 2)  /* if they're both odd or both even */
	   && rooti != rootj )             /* and they're currently not in the same subset */
        unionDS( ds, rooti, rootj );        /* then link them together */
    }

  printf("Done linking ALL the ODDS together and the EVENS together.\n");

  /* see what the root of the even's and odd's are */
  even_root = findDS( ds, 0 ) ;
  odd_root = findDS( ds, 1 );


  /* now check that the above joining resulted in 2 disjoint sets */
  /* every integer must either belong to the EVENS or the ODDS  */
  for(i = 0 ; i < NUM_INSERTS ; i++)
  {
    root = findDS( ds , data[i] );

    if ( data[i] % 2 == 0 )
      assert( root == even_root);
    else
      assert( root == odd_root);
  }

  printf("Done checking that every integer either belonged to the ODDS or the EVENS.\n");

  freeDS(&ds) ;
  if ( ds != NULL ) 
	printf ( "\nError: I expected a NULL value for ds here\n" ) ;
  else {
  	printf("Done freeing the DisjointSubsets.\n");
  	printf("\nDone. No obvious errors found.   Nice Work\n");
  }
  return 0;
}


int main()
{
  return TheTestDS() ;
}
