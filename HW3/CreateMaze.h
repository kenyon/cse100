
/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
 *
 *	CreateMaze.h			Spring 2008
 *
 *
 * = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */

#include "List.h"
#include "PQueue.h"

List *
create_maze ( unsigned int r , unsigned int c , PQueue pq ) ;
