#include <string.h>

typedef struct {
	unsigned int room1 , room2 ;
} Wall ;

void *
copyWall ( void * w1 , void * w2 ) 
{
	memcpy ( w1 , w2 , sizeof(Wall) ) ;
	return w1 ;
}
