/** @file
 * Contains create_maze() which creates a maze. This is done by creating a list
 * of removed walls.
 *
 * @author Kenyon Ralph <kralph@ucsd.edu>
 * @date due 20080606
 */

#include "DisjointSubsets.h"
#include "List.h"
#include "PQueue.h"
#include "Wall.h"

/** Creates a maze. Removes walls with the highest demolition priorities.
 * Removes just enough walls so each room can be reached from any other in
 * exactly one way.
 *
 * So, this function selects r * c - 1 walls for removal. Walls are removed in
 * the order given by the priority queue. However, a wall between two rooms is
 * never removed if there is already a path between them.
 *
 * Disjoint subsets each denote connected rooms.
 *
 * @param[in] r number of rows
 * @param[in] c number of columns
 * @param[in] pq a priority queue of Wall objects, representing the wall
 * demolition probabilities.
 *
 * @return a List of Wall records representing the walls to be demolished
 */
List *create_maze(unsigned int r, unsigned int c, PQueue pq)
{
    Wall wall;

    // I don't know why this function returns a pointer to a List rather than
    // simply a List, but that's the way the professor provided it to us.
    List *to_destroy = makeList(sizeof(Wall), copyWall, free);

    // The argument here is the total number of walls in a maze, excluding the
    // walls on the outer edges.
    DisjointSubsets ds = makeDisjointSubsets((c - 1) * r + (r - 1) * c);

    unsigned int num_to_remove = r * c - 1;
    unsigned int name1, name2;

    while (num_to_remove > 0) {
	fetchPQ(pq, &wall);
	name1 = findDS(ds, wall.room1);
	name2 = findDS(ds, wall.room2);

	if (name1 != name2) {
	    insertHead(to_destroy, &wall);
	    num_to_remove--;
	    unionDS(ds, name1, name2);
	}
    }

    freeDS(&ds);

    return to_destroy;
}

/* vim: set si ai noet sts=1 tw=0 sw=4: */
