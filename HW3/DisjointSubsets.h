/* * * * * * * * * * * * * * * * * * * * * * * * * 
 *
 *	Disjoint Subsets union-find 	Spring 2008
 *
 *      Walt Burkhard
 *
 * * * * * * * * * * * * * * * * * * * * * * * * */ 

#ifndef UNIVERSE 
#define UNIVERSE 

#include <stdbool.h>

typedef void *DisjointSubsets;

DisjointSubsets makeDisjointSubsets(unsigned int);
void freeDS(DisjointSubsets*);
unsigned int findDS(DisjointSubsets, unsigned int);
bool unionDS(DisjointSubsets, unsigned int, unsigned int);
unsigned int testDS(DisjointSubsets, unsigned int);

// Below stuff added by Kenyon.
unsigned int get_DS_count(DisjointSubsets);
unsigned int get_height_at(DisjointSubsets, unsigned int);
unsigned int get_parent_at(DisjointSubsets, unsigned int);

#endif
