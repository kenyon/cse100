/** @file
 * Contains mazeSolve() which solves a maze.
 *
 * @author Kenyon Ralph <kralph@ucsd.edu>
 * @date due 20080606
 */

#include <stdbool.h>
#include <stdlib.h>

#include "Int.h"
#include "List.h"

/** The maze solution path, where the list head is the maze entry and the tail
 * is the maze goal.
 */
static List solution_path;

/** An array with indexes corresponding to rooms of the maze. True if the room
 * has been visited, false otherwise.
 */
static bool *marks;

/** True if the goal has been reached.
 */
static bool goal_reached = false;

/** Depth-first search of the maze graph, searching for a path to the "goal"
 * room.
 *
 * @param[in] room the room we're starting a search from
 * @param[in] goal the room at the end of the maze
 * @param[in] r the number of rows in the maze
 * @param[in] c the number of columns in the maze
 * @param[in] row same as in mazeSolve()
 * @param[in] column same as in mazeSolve()
 */
static void depthFirstSearch(unsigned int room, unsigned int goal, bool *row, unsigned int r, bool *column, unsigned int c)
{
    unsigned int temp;

    marks[room] = true;

    if (room == goal) {
	goal_reached = true;
	return;
    }

    // try east

    if (room % c < c - 1) {
	temp = room + 1;

	if (marks[temp] == false && column[room] == false) {
	    insertHead(solution_path, makeInt(temp));
	    depthFirstSearch(temp, goal, row, r, column, c);

	    if (goal_reached == true) {
		return;
	    }

	    deleteHead(solution_path, NULL);
	}
    }

    // try west

    if (room % c > 0) {
	temp = room - 1;

	if (marks[temp] == false && column[temp] == false) {
	    insertHead(solution_path, makeInt(temp));
	    depthFirstSearch(temp, goal, row, r, column, c);

	    if (goal_reached == true) {
		return;
	    }

	    deleteHead(solution_path, NULL);
	}
    }

    // try south

    if (room / c > 0) {
	temp = room - c;

	if (marks[temp] == false && row[temp] == false) {
	    insertHead(solution_path, makeInt(temp));
	    depthFirstSearch(temp, goal, row, r, column, c);

	    if (goal_reached == true) {
		return;
	    }

	    deleteHead(solution_path, NULL);
	}
    }

    // try north

    if (room / c < r - 1) {
	temp = room + c;

	if (marks[temp] == false && row[room] == false) {
	    insertHead(solution_path, makeInt(temp));
	    depthFirstSearch(temp, goal, row, r, column, c);

	    if (goal_reached == true) {
		return;
	    }

	    deleteHead(solution_path, NULL);
	}
    }
}

/** Solves a maze. Conducts a depth-first search following removed walls to
 * determine the path from entry to goal. Invoked by the solver.c program.
 *
 * @param[in] entry a room between 0 and c - 1. That is, a room along the lower
 * edge.
 * @param[in] goal a room between (r - 1) * c and r * c - 1. That is, a room
 * along the top edge.
 * @param[in] row row[i] is false if the wall between rooms i and i + c is
 * removed; true otherwise
 * @param[in] column column[i] is false if the wall between rooms i and i + 1
 * is removed; true otherwise
 * @param[in] r the number of rows in the maze
 * @param[in] c the number of columns in the maze
 *
 * @return a List containing Int objects which room numbers. The order of the
 * list, from head to tail, represents the path, from entry to goal.
 */
List mazeSolve(unsigned int entry, unsigned int goal, bool *row, unsigned int r, bool *column, unsigned int c)
{
    marks = (bool*)calloc(r * c, sizeof(bool));

    solution_path = makeList(sizeofInt(), copyInt, freeInt);

    depthFirstSearch(entry, goal, row, r, column, c);

    free(marks);

    return solution_path;
}

/* vim: set si ai noet sts=1 tw=0 sw=4: */
