#include <stdio.h>
#include <stdlib.h>

#include "List.h"
#include "PQueue.h"
#include "CreateMaze.h"
#include "Wall.h"

int
main ( void )
{
	char name [100] ; 
	int r , c , i , j , entry ; 
	int goalc ; 

	List maze ;		// stack of walls to be demolished. 
	Wall wall ;
	PQueue pq ;

	FILE * fptr ; 

	printf( "maze creator\n  number of rows? " ) ;
	scanf ( "%d" , &r ) ;

	printf ( "  number of columns " ) ;
	scanf ( "%d" , &c ) ;

	printf ( "entry along bottom edge:  0 <= entry < columns " ) ;
	scanf ( "%d" , &entry ) ;

	printf ( "exit along top edge:  0 <= goal < columns " ) ;
	scanf ( "%d" , &goalc ) ;

	printf ( "output file name: " ) ;
	scanf ( "%s" , name ) ;
	fptr = fopen ( name , "w+" ) ;
	if ( fptr == NULL ) {
		fprintf ( stderr , "can't open output file\n" ) ;
		exit(0) ;
	}

	srandom ( 0 ) ;

	fprintf ( fptr , "%s\t%d rows x %d columns\n" , name , r , c ) ;
	fprintf ( fptr , "start\t%d\n" , entry ) ;
	fprintf ( fptr , "end\t%d\t%d\n" , goalc, goalc+(r-1)*c ) ;

	pq = makePQueue ( 2*r*c , sizeof(Wall) , copyWall , free ) ;

	for ( i = 0 ; i < r ; i++ ) {
		for ( j = 0 ; j < c ; j++ ) {
			wall.room1 = i*c+j ;
			if ( j < c-1 ) {
				wall.room2 = i*c+1+j ;
				insertPQ ( pq , &wall ,(double)random() ) ;
			}
			if ( i < r-1 ) {
				wall.room2 = (i+1)*c+j ;
				insertPQ ( pq , &wall ,(double)random() ) ;
			}
		}
	}

	maze = create_maze ( r , c , pq ) ;

	while ( pop ( maze , & wall ) ) 
		fprintf ( fptr , "%d %d\n" , wall.room1 , wall.room2 ) ;
	freeList(maze);
	fflush ( fptr ) ;
	freePQ ( & pq ) ;

	return EXIT_SUCCESS;
}
