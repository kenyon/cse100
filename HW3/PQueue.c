/** @file
 * Priority queue implementation for CSE 100 homework 3. Implements the
 * Floyd-Williams algorithm from the Spring 2008 CSE 100 notes.
 *
 * @author Kenyon Ralph <kralph@ucsd.edu>
 * @date due 20080606
 */

#include <stdbool.h>
#include <stdlib.h>

/** The data type being stored in the priority queue.
 */
typedef void *DataPQ;

/** The Floyd-Williams priority queue structure.
 */
typedef struct PQueue {
    /** An array of pointers to the data which is being stored in the priority
     * queue.
     */
    DataPQ *slots;

    /** Priorities of the data. This is an array paralleling the slots array.
     */
    double *priorities;

    /** Floyd-Williams array size, that is, the number of elements in
     * PQueue.slots. The maximum PQueue.count is sizeArray - 1 because the data
     * are inserted beginning at index 1. Index 0 of PQueue.slots is just a
     * wasted slot. Oh well.
     */
    size_t sizeArray;

    /** The size of data items.
     */
    size_t sizeData;

    /** A count of the number of items actually stored in the structure. This
     * may be different from PQueue.sizeArray. If PQueue.sizeArray - 1 ==
     * PQueue.count, then the priority queue is full.
     */
    unsigned int count;

    /** Pointer to a function for copying data items.
     */
    DataPQ (*copyData)();

    /** Pointer to a function for freeing data items.
     */
    void (*freeData)();
} *PQueue;

/** Makes a priority queue.
 *
 * @param[in] maxElements the maximum number of elements you plan to store
 * within the priority queue
 * @param[in] sizeData the size of the data objects to be stored in the
 * priority queue
 * @param[in] copyData a copy function for the data objects
 * @param[in] freeData a free function for the data objects
 *
 * @return the new priority queue
 */
PQueue makePQueue(unsigned int maxElements, size_t sizeData, DataPQ (*copyData)(), void (*freeData)())
{
    PQueue pq = (PQueue)malloc(sizeof(struct PQueue));

    pq->sizeArray = maxElements + 1;
    pq->sizeData = sizeData;

    pq->slots = (DataPQ*)calloc(pq->sizeArray, sizeof(DataPQ));
    unsigned int i;
    for (i = 1; i < pq->sizeArray; i++) {
	pq->slots[i] = (DataPQ)malloc(pq->sizeData);
    }

    pq->priorities = (double*)calloc(pq->sizeArray, sizeof(double));

    pq->count = 0;
    pq->copyData = copyData;
    pq->freeData = freeData;

    return pq;
}

/** Retrieves the highest priority data item from the queue and copies it to
 * the data parameter.
 *
 * @param[in] pq the priority queue to fetch from
 * @param[out] data a data item to which the highest priority item from the
 * queue will be copied
 *
 * @return true if a data item is fetched, false otherwise
 */
bool fetchPQ(PQueue pq, DataPQ data)
{
    if (pq->count == 0 || data == NULL) {
	return false;
    }

    pq->copyData(data, pq->slots[1]);

    unsigned int i, j;
    for (i = 1, j = 2; j <= pq->count; i = j, j = 2 * i) {
	if (j < pq->count && pq->priorities[j] < pq->priorities[j + 1]) {
	    j++;
	}

	if (pq->priorities[pq->count] > pq->priorities[j]) {
	    break;
	}

	pq->priorities[i] = pq->priorities[j];
	pq->copyData(pq->slots[i], pq->slots[j]);
    }

    pq->priorities[i] = pq->priorities[pq->count];
    pq->copyData(pq->slots[i], pq->slots[pq->count]);
    pq->count--;

    return true;
}

/** Inserts a copy of a data object into the priority queue.
 *
 * @param[in,out] pq the priority queue to insert into
 * @param[in] data the data item to copy and insert
 * @param[in] priority the priority of the data item to insert
 *
 * @return true if successful, false otherwise
 */
bool insertPQ(PQueue pq, DataPQ data, double priority)
{
    if (pq == NULL || pq->count >= pq->sizeArray - 1 || data == NULL) {
	return false;
    }

    unsigned int i;
    for (i = ++pq->count; i > 1 && pq->priorities[i / 2] < priority; i /= 2) {
	pq->priorities[i] = pq->priorities[i / 2];
	pq->copyData(pq->slots[i], pq->slots[i / 2]);
    }

    pq->priorities[i] = priority;
    pq->copyData(pq->slots[i], data);

    return true;
}

/** Frees the contents of the priority queue pointed to by pqPtr and sets the
 * pointer to NULL.
 *
 * @param[in,out] pqPtr pointer to the priority queue to destroy and return to
 * the heap
 */
void freePQ(PQueue *pqPtr)
{
    unsigned int i;
    for (i = 0; i < (*pqPtr)->sizeArray; i++) {
	(*pqPtr)->freeData((*pqPtr)->slots[i]);
    }
    free((*pqPtr)->slots);

    free((*pqPtr)->priorities);

    free(*pqPtr);
    *pqPtr = NULL;

    return;
}

/* vim: set si ai noet sts=1 tw=0 sw=4: */
