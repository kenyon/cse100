#ifndef PQUEUE
#define PQUEUE

#include <stdbool.h>
#include <stdlib.h>

typedef void *PQueue;
typedef void *DataPQ;

PQueue makePQueue(unsigned int, size_t, DataPQ (*copyData)(), void (*freeData)());
bool fetchPQ(PQueue, DataPQ);
bool insertPQ(PQueue, DataPQ, double);
void freePQ(PQueue*);

#endif
