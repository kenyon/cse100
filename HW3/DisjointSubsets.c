/** @file
 * Disjoint subsets (also known as union find) implementation for CSE 100
 * homework 3. Implements link-by-height and path compression heuristics.
 *
 * @author Kenyon Ralph <kralph@ucsd.edu>
 * @date due 20080606
 */

#include <stdbool.h>
#include <stdlib.h>

typedef struct DisjointSubsets {
    /** Parents array.
     */
    unsigned int *parents;

    /** Heights array.
     */
    unsigned int *heights;

    /** The number of elements in the DisjointSubsets.nodes array.
     */
    unsigned int count;
} *DisjointSubsets;

/** Creates and initializes an array of nodes so that each element is within a
 * singleton set.
 *
 * @param[in] numElements the number of elements in the universe
 *
 * @return the new DisjointSubsets object
 */
DisjointSubsets makeDisjointSubsets(unsigned int numElements)
{
    DisjointSubsets ds = (DisjointSubsets)malloc(sizeof(struct DisjointSubsets));

    ds->count = numElements;

    ds->parents = (unsigned int*)calloc(ds->count, sizeof(unsigned int));
    ds->heights = (unsigned int*)calloc(ds->count, sizeof(unsigned int));

    unsigned int i;
    for (i = 0; i < ds->count; i++) {
	ds->parents[i] = i;
	ds->heights[i] = 0;
    }

    return ds;
}

/** Returns the number of elements in the given DisjointSubsets object.
 *
 * @param[in] ds the DisjointSubsets object to look in
 *
 * @return the number of elements in the given DisjointSubsets object
 */
unsigned int get_DS_count(DisjointSubsets ds)
{
    return ds->count;
}

/** Returns the value of the parents array at the given index.
 *
 * @param[in] ds the DisjointSubsets object to look in
 * @param[in] index the index of the element to retrieve
 *
 * @return the value of the parents array at the given index
 */
unsigned int get_parent_at(DisjointSubsets ds, unsigned int index)
{
    return ds->parents[index];
}

/** Returns the value of the heights array at the given index.
 *
 * @param[in] ds the DisjointSubsets object to look in
 * @param[in] index the index of the element to retrieve
 *
 * @return the value of the heights array at the given index
 */
unsigned int get_height_at(DisjointSubsets ds, unsigned int index)
{
    return ds->heights[index];
}

/** Frees the union find structure and sets the pointer to NULL.
 *
 * @param[in,out] DSptr pointer to the DisjointSubsets object to free
 */
void freeDS(DisjointSubsets *DSptr)
{
    free((*DSptr)->parents);
    free((*DSptr)->heights);

    free(*DSptr);
    *DSptr = NULL;

    return;
}

/** Accesses item element within the structure, then returns the index of the
 * root of the tree containing the element. Implements the path compression
 * heuristic, so the structure is actually changed during this find operation.
 *
 * @param[in,out] ds the DisjointSubsets object to look in
 * @param[in] element the element to look for
 *
 * @return the index of the root of the tree containing the element
 */
unsigned int findDS(DisjointSubsets ds, unsigned int element)
{
    if (element != ds->parents[element]) {
	ds->parents[element] = findDS(ds, ds->parents[element]);
    }

    return ds->parents[element];
}

/** Combines two sets by changing a parent index. The link-by-height heuristic
 * is implemented here. If the trees have identical sizes, the left argument
 * becomes the root of the combined tree.
 *
 * @param[in,out] ds the DisjointSubsets object to work with
 * @param[in] left the left set to combine
 * @param[in] right the right set to combine
 *
 * @return true if the labels designated current subsets and the linkage
 * occurred; false otherwise
 */
bool unionDS(DisjointSubsets ds, unsigned int left, unsigned int right)
{
    if (left >= ds->count || right >= ds->count) {
	return false;
    }

    if (ds->heights[left] > ds->heights[right]) {
	ds->parents[right] = left;
    } else {
	ds->parents[left] = right;
    }

    if (ds->heights[left] == ds->heights[right]) {
	ds->heights[right]++;
    }

    return true;
}

/** Returns the number of parent links traversed from the given element to
 * root. This is a method for testing the DisjointSubsets structure.
 *
 * @param[in] ds the DisjointSubsets object to look at
 * @param[in] element the element to look for
 *
 * @return the number of parent links traversed from the given element to root
 */
unsigned int testDS(DisjointSubsets ds, unsigned int element)
{
    unsigned int parent_count = 0;

    while (element != ds->parents[element]) {
	element = ds->parents[element];
	parent_count++;
    }

    return parent_count;
}

/* vim: set si ai noet sts=1 tw=0 sw=4: */
