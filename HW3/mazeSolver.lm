mazeSolver.lm: Logic manual for mazeSolver.h and mazeSolver.c
A maze solver

Author of this file and mazeSolver.c: Kenyon Ralph <kralph@ucsd.edu>
UCSD CSE 100, Spring 2008. Assignment due 20080606.

For additional documentation, run Doxygen <http://doxygen.org/> on mazeSolver.c
to extract and process into a nice format the in-code documentation that I
wrote.

mazeSolver.h is included by solver.c.

Algorithm
=========

The mazeSolve() function solves a maze. By "solve" I mean creates a list of
rooms denoting the path from the start to finish of the maze. This is done by a
depth-first search on the maze. A maze can be visualized as a tree, with the
root at the start and a bunch of leaves, but only one leaf being the end of the
maze. This is why a depth-first search works.

The depth-first search is implemented recursively: starting from a room, the
algorithm looks for the maze finish down each direction away from that room.

The algorithm prevents infinite looping by maintaining an array of boolean
values corresponding to each room of the maze (the marks array). If a room has
been visited, its value in the marks array is set to true.

Some cleverness has to be used to calculate the four directions. These are the
north, south, east, and west "if" statements.

There are also arrays of booleans passed to mazeSolve() which indicate whether
a wall has been removed between adjacent rooms, and these arrays are used in
the depth-first search to determine whether searching through a particular room
is valid (i.e., we want to generate a path that does not magically pass through
walls).

Variables
=========

/** The maze solution path, where the list head is the maze entry and the tail
 * is the maze goal.
 */
static List solution_path;

/** An array with indexes corresponding to rooms of the maze. True if the room
 * has been visited, false otherwise.
 */
static bool *marks;

/** True if the goal has been reached.
 */
static bool goal_reached = false;

Functions
=========

/** Depth-first search of the maze graph, searching for a path to the "goal"
 * room.
 *
 * @param[in] room the room we're starting a search from
 * @param[in] goal the room at the end of the maze
 * @param[in] r the number of rows in the maze
 * @param[in] c the number of columns in the maze
 * @param[in] row same as in mazeSolve()
 * @param[in] column same as in mazeSolve()
 */
static void depthFirstSearch(unsigned int room, unsigned int goal, bool *row, unsigned int r, bool *column, unsigned int c)

/** Solves a maze. Conducts a depth-first search following removed walls to
 * determine the path from entry to goal. Invoked by the solver.c program.
 *
 * @param[in] entry a room between 0 and c - 1. That is, a room along the lower
 * edge.
 * @param[in] goal a room between (r - 1) * c and r * c - 1. That is, a room
 * along the top edge.
 * @param[in] row row[i] is false if the wall between rooms i and i + c is
 * removed; true otherwise
 * @param[in] column column[i] is false if the wall between rooms i and i + 1
 * is removed; true otherwise
 * @param[in] r the number of rows in the maze
 * @param[in] c the number of columns in the maze
 *
 * @return a List containing Int objects which room numbers. The order of the
 * list, from head to tail, represents the path, from entry to goal.
 */
List mazeSolve(unsigned int entry, unsigned int goal, bool *row, unsigned int r, bool *column, unsigned int c)
