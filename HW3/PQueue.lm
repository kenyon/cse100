PQueue.lm: Logic manual for PQueue.h and PQueue.c
A priority queue implementation

Author of this file and PQueue.c: Kenyon Ralph <kralph@ucsd.edu>
UCSD CSE 100, Spring 2008. Assignment due 20080606.

For additional documentation, run Doxygen <http://doxygen.org/> on PQueue.c to
extract and process into a nice format the in-code documentation that I wrote.

PQueue.h is included by CreateMaze.c, CreateMaze.h, and maze.c.

Algorithm
=========

A priority queue is like a regular queue, but it has a priority associated with
each element. Unlike a regular queue, items are not necessarily removed in FIFO
order. Instead, they are removed according to their priorities: highest
priorities are removed first.

The Floyd-Williams implementation is a tree implementation with heap ordering. The tree is stored in a standard C array. The tree root is at index 1 of the array.

The insertion algorithm works by looking at the first free array slot, and
looking up the tree until the priority of the item to be inserted is greater
than or equal to the item being looked at. The tree items are swapped as the
tree is climbed. Finally, the item is inserted in the correct location,
maintaining heap order.

The fetch algorithm works by first returning the root of the tree, which is the
element with the highest priority. Then heap order is restored if necessary by
placing the last item as the root, and iteratively comparing and swapping down
the branches of the tree.

Structure
=========

/** The data type being stored in the priority queue.
 */
typedef void *DataPQ;

/** The Floyd-Williams priority queue structure.
 */
typedef struct PQueue {
    /** An array of pointers to the data which is being stored in the priority
     * queue.
     */
    DataPQ *slots;

    /** Priorities of the data. This is an array paralleling the slots array.
     */
    double *priorities;

    /** Floyd-Williams array size, that is, the number of elements in
     * PQueue.slots. The maximum PQueue.count is sizeArray - 1 because the data
     * are inserted beginning at index 1. Index 0 of PQueue.slots is just a
     * wasted slot. Oh well.
     */
    size_t sizeArray;

    /** The size of data items.
     */
    size_t sizeData;

    /** A count of the number of items actually stored in the structure. This
     * may be different from PQueue.sizeArray. If PQueue.sizeArray - 1 ==
     * PQueue.count, then the priority queue is full.
     */
    unsigned int count;

    /** Pointer to a function for copying data items.
     */
    DataPQ (*copyData)();

    /** Pointer to a function for freeing data items.
     */
    void (*freeData)();
} *PQueue;

Methods
=======

/** Makes a priority queue.
 *
 * @param[in] maxElements the maximum number of elements you plan to store
 * within the priority queue
 * @param[in] sizeData the size of the data objects to be stored in the
 * priority queue
 * @param[in] copyData a copy function for the data objects
 * @param[in] freeData a free function for the data objects
 *
 * @return the new priority queue
 */
PQueue makePQueue(unsigned int maxElements, size_t sizeData, DataPQ (*copyData)(), void (*freeData)())

/** Retrieves the highest priority data item from the queue and copies it to
 * the data parameter.
 *
 * @param[in] pq the priority queue to fetch from
 * @param[out] data a data item to which the highest priority item from the
 * queue will be copied
 *
 * @return true if a data item is fetched, false otherwise
 */
bool fetchPQ(PQueue pq, DataPQ data)

/** Inserts a copy of a data object into the priority queue.
 *
 * @param[in,out] pq the priority queue to insert into
 * @param[in] data the data item to copy and insert
 * @param[in] priority the priority of the data item to insert
 *
 * @return true if successful, false otherwise
 */
bool insertPQ(PQueue pq, DataPQ data, double priority)

/** Frees the contents of the priority queue pointed to by pqPtr and sets the
 * pointer to NULL.
 *
 * @param[in,out] pqPtr pointer to the priority queue to destroy and return to
 * the heap
 */
void freePQ(PQueue *pqPtr)
