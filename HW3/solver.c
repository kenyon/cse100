#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Int.h"
#include "List.h"
#include "mazeSolver.h"

int
main ( int argc , char ** argv )
{
	char name [100] , buffer[100] ; 
	int goalc , rr , cc , entry , i ; 

	bool * row , * column ; 	// row[i] indicates the wall status between room i and i+c    
					// column[i] indicates the wall status between room i and i+1
	int r , c , goal ; 		// r rows and c columns		goal is the exit room
	List solution ;			// temporary solution goes here -- before it is written to fptr1 file.
	FILE * fptr , * fptr1 ; 
	Int room; 			// temporary room index.

	if ( argc != 2 ) {
		fprintf ( stderr , "%s <name of maze>\n" , argv[0] ) ;
		exit ( 1 ) ;
	} else {
		strcpy ( name , argv[1] ) ;
		fptr = fopen ( name , "r" ) ;
		if ( fptr == NULL ) {
			fprintf ( stderr , "trouble opening %s\n" , name ) ;
			exit ( 1 ) ;
		}
	}
	fscanf ( fptr , "%[^\n]%*c" , buffer ) ; 
	sscanf ( buffer , "%*s %d rows x %d columns" , &r , &c ) ;
	
	fptr1 = fopen (  strcat (  name , ".solved" ) , "w+" ) ;
	if ( fptr1 == NULL ) {
		fprintf ( stderr , "trouble opening %s\n" , name ) ;
		exit ( 1 ) ;
	}
	
	fscanf ( fptr , "%[^\n]%*c" , buffer ) ; 
	sscanf ( buffer , "start\t%d" , &entry ) ;
	fscanf ( fptr , "%[^\n]%*c" , buffer ) ; 
	sscanf ( buffer , "end\t%d\t%d" , &goalc, &goal ) ;


	row = (bool *) malloc ( sizeof(bool )*r*c ) ;		// represents the maze structure
	column = (bool *) malloc ( sizeof(bool )*c*r ) ;		// 

	for ( i = 0 ; i < r*c ; i++ )  	
		column[i] = row[i] = true ; 				// row and column walls present.

	for ( i = r*c-2 ; i >= 0 ; i-- ) { 		// read the data 
		fscanf ( fptr , "%d %d " , &rr , &cc ) ;
		if ( rr + c == cc )
			row[rr] = false ;				// North wall missing
		else
			column[rr] = false ;				// East wall missing
	}

	fclose ( fptr ) ; 
	
	// create path through the maze using depth first search.

	solution = mazeSolve ( entry , goal , row , r , column , c ) ;	// puts rooms into the solution, entry to goal.
	
	room = makeInt(0) ;
	fprintf ( fptr1 , "start %d " , entry ) ;			// writes solution to file 
	while ( deleteTail ( solution , room )  == true ) 
		fprintf ( fptr1 , "%d " , valueInt ( room ) ) ;
	fprintf ( fptr1 , "end\n" ) ;
	fprintf ( stderr , "%s created\n" , name ) ;

	free(row);
	free(column);
	freeInt(room);
	freeList(solution);

	return EXIT_SUCCESS;
}
