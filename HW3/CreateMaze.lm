CreateMaze.lm: Logic manual for CreateMaze.h and CreateMaze.c
A maze creator

Author of this file and CreateMaze.c: Kenyon Ralph <kralph@ucsd.edu>
UCSD CSE 100, Spring 2008. Assignment due 20080606.

For additional documentation, run Doxygen <http://doxygen.org/> on CreateMaze.c
to extract and process into a nice format the in-code documentation that I
wrote.

CreateMaze.h is included by maze.c.

Algorithm
=========

The create_maze() function creates a maze by returning a List of walls to be
removed from the maze grid. The walls are removed in an order given by a
priority queue. The DisjointSubsets methods are used to first make sure the
rooms are in different sets before listing that wall for removal. If the wall
is indeed removed, the two rooms in question are unionized. The effect of this
procedure is that only one path will exist between any two rooms.

Function
========

/** Creates a maze. Removes walls with the highest demolition priorities.
 * Removes just enough walls so each room can be reached from any other in
 * exactly one way.
 *
 * So, this function selects r * c - 1 walls for removal. Walls are removed in
 * the order given by the priority queue. However, a wall between two rooms is
 * never removed if there is already a path between them.
 *
 * Disjoint subsets each denote connected rooms.
 *
 * @param[in] r number of rows
 * @param[in] c number of columns
 * @param[in] pq a priority queue of Wall objects, representing the wall
 * demolition probabilities.
 *
 * @return a List of Wall records representing the walls to be demolished
 */
List *create_maze(unsigned int r, unsigned int c, PQueue pq)
