
#include  <stdio.h>
#include  <math.h>

unsigned 
nextprime ( unsigned s ) 
{

	unsigned mysize , j , k ; 

	if ( s % 2 == 0 ) 
		s++ ;
	for ( mysize = s ; ; mysize += 2 ) {
		j  =  (unsigned)pow((double)mysize,0.5) ;	
		for ( k = 3 ; k <= j ; k += 2 ) 
			if ( mysize%k == 0 ) break ; // not a prime
		if ( mysize%k == 0 ) 
			continue ;
		else 
			return  mysize  ;
	}
}
