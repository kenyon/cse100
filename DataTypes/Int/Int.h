/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
 *
 * 	Int class		everything hidden 
 *
 * 	Spring 2008		
 * 
 * = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */
#ifndef INT 
#define INT

typedef void * Int ; 			// structure hidden 

Int 
copyInt ( Int a , Int b ) ;		// make a copy of Int b in Int a
					// both created elsewhere; returns a 
					// if successful and NULL otherwise.

Int makeInt ( int p ) ;		// creates an Int structure within heap
				// Int initialized with value p

void updateInt ( Int ii , int p ) ;	// assigns Int ii value p.

unsigned hashInt ( Int ) ;		// determines a hash value of Int

void freeInt ( Int ) ;			// returns Int to heap

int diffInt ( Int a , Int b ) ;		// returns a - b

int sizeofInt ( void ) ;		// returns the size of an Int structure
					// in bytes

int valueInt ( Int ) ;			// returns integer value of Int

#endif
