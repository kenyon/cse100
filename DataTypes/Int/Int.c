/* = = = = = = = = = = = = = = = = = = = = = = = = = = = =
 *
 *	Int.c		Spring 2008.
 *
 * = = = = = = = = = = = = = = = = = = = = = = = = = = = = */

#include <stdlib.h>

typedef struct Int {
	int _i ;
} * Int ;

Int makeInt ( int t1 ) 
{
 	Int temp =  ( Int ) malloc ( sizeof( struct Int ) ) ; 
	if ( temp != NULL )
		temp->_i = t1 ;
	return temp ;
}

void
updateInt ( Int t , int t1 ) 
{
	t->_i = t1 ; 
}

Int 
copyInt ( Int e1 , Int e2 ) 
{
	if ( e1 == NULL || e2 == NULL ) return NULL ;
	e1->_i = e2->_i ;
	return e1 ;
}

unsigned
hashInt ( Int e1 ) 
{
	return e1->_i ;
}

int 
diffInt ( Int e1 , Int e2 ) 
{
	return e1->_i - e2->_i ;
}

void 			// 	only valid for e returned from malloc!  
freeInt ( Int e ) 
{
	free ( e ) ;
}

int
sizeofInt ( void ) 
{
	return ( sizeof ( struct Int ) ) ;
}

int
valueInt ( Int a ) 
{
	return a->_i ; 
}
