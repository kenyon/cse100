/* * * * * * * * * * * * * * * * * * * * * * * *
 *
 *     List Data type 		Spring 2008
 *
 *     Walt Burkhard		List source file
 *
 * * * * * * * * * * * * * * * * * * * * * * * */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef DATA
#define DATA
typedef void * Data ;
#endif

typedef struct ListNode {
	struct ListNode * next ; 	/* reference to following ListNode */
	struct ListNode * previous ;	/* reference to preceding ListNode */
	Data dptr ;
} ListNode ;

	
typedef struct List {
	ListNode * head ; 		/* Head end of List		*/
	ListNode * tail ;		/* Tail end of List		*/
	int _lengthList ;		/* number of items within List	*/
	int _sizeData ;			/* byte size of data to be stored */
	void (*_freeData)( ) ;		/* returns data item to heap	*/
	void * (*_copyData)( ) ;	/* copies data item to another	*/
} * List ;

typedef struct ListIt {
	ListNode * which ;		/* points to next node in a traversal */
	List list ;			/* identifies the list 		*/
} * ListIt ;

List 
makeList ( int sizeData  , void * (*copy)() , void (*FreeData)() )
{
	List temp = (List) malloc ( sizeof ( struct List ) ) ;
	if ( temp == NULL ) {
	  /* We ran out of memory; if we were debugging, we'd put a printf here. */
	  /* fprintf ( stderr , ">>> trouble creating a List\n" ) ; */
	  return NULL ;
	}
	temp->_sizeData = sizeData ;
	temp->head = temp->tail = NULL ;
	temp->_lengthList = 0 ;
	temp->_copyData = copy ;
	temp->_freeData = FreeData ;
	return temp ;
}

void 
clearList ( List list ) 
{
	ListNode * temp ;
	if ( list == NULL )  return ;
		
	while ( list -> _lengthList > 0 ) {
		temp = list->head ;
		list->_lengthList -- ;
		if ( ( list->head = temp->next ) == NULL ) 
			list->tail = NULL ;
		list->_freeData( temp->dptr ) ;
		free ( temp ) ;
	}
	return ;
}

void
freeList ( List list ) 
{
	clearList ( list ) ;
	free ( list ) ;
}


bool
deleteHead ( List list , Data data )
{
	ListNode * temp ;
	if ( list == NULL || list->_lengthList <= 0 || 
			( temp = list->head ) == NULL ) 
		return false ;
	if ( ( list->head = temp->next ) == NULL ) 
		list->tail = NULL ;
	else 
		list->head->previous = NULL ;
	list->_copyData( data , temp->dptr ) ;	
	list->_lengthList -- ;
	list->_freeData( temp->dptr ) ;
	free ( temp ) ;
	return true ;
}


bool 
insertHead ( List list , Data data ) 
{
	ListNode * temp ;
	if ( list == NULL ) 
		return false ;
	temp = (ListNode *) malloc ( sizeof(ListNode) ) ;
	if ( temp == NULL ) 
		return false ;
	temp->dptr = ( char * ) calloc ( 1 , list->_sizeData ) ;
	if ( temp->dptr == NULL ) 
		return false ;

	temp->next = list->head ;    
	temp->previous = NULL ;
	list->_copyData( temp->dptr , data ) ;
	list->_lengthList ++ ;

	if ( list->tail == NULL ) 
		list->tail = temp ;
	else 
		list->head->previous = temp ;
	list->head = temp ;
	return true ;
}

bool 
insertTail ( List list , Data data ) 
{
	ListNode * temp ;
	if ( list == NULL ) 
		return false ;
	temp = (ListNode *) malloc ( sizeof(ListNode) ) ;
	if ( temp == NULL ) 
		return false ;
	temp->dptr = ( char * ) calloc ( 1 , list->_sizeData ) ;

	if ( temp->dptr == NULL ) 
		return false ;

	temp->previous = list->tail ;   
	temp->next = NULL ;
	list->_copyData( temp->dptr , data ) ;
	list->_lengthList ++ ;

	if ( list->head == NULL ) 
		list->head = temp ;
	else 
		list->tail->next = temp ;
	list->tail = temp ;
	return true ;
}

bool 
deleteTail ( List list , Data data ) 
{
	ListNode * temp ;
	if ( list == NULL || list->_lengthList <= 0  || 
			( temp = list->tail ) == NULL ) 
		return false ;
	if ( ( list->tail = temp->previous ) == NULL ) 
		list->head = NULL ;
	else 
		list->tail->next = NULL ;
	list->_copyData( data , temp->dptr ) ;	
	list->_lengthList -- ;
	list->_freeData( temp->dptr ) ;
	free ( temp ) ;
	return true ;
}

bool
accessTail ( List list , Data data ) 
{
	if ( list == NULL || list->tail == NULL || list->tail->dptr == NULL ) 
		return false ;
	list->_copyData( data , list->tail->dptr ) ;	
	return true ;
}

bool
accessHead ( List list , Data data ) 
{
	if ( list == NULL || list->head == NULL || list->head->dptr == NULL ) 
		return false ;
	list->_copyData( data , list->head->dptr ) ;	
	return true ;
}

bool
accessList ( List list , Data data , long index ) 
{
	int count ;
	ListNode * temp ;

	if ( list == NULL || index >= list->_lengthList || index < 0 ) 
		return false ;
	temp = list->head ;
	for ( count = 0 ; count < index ; count++ )  
		temp = temp->next ; 			// linear search.
	list->_copyData( data , temp->dptr ) ;
	return true ;
}

List
listConcatenate ( List list1 , List list2 ) 		
{
	if ( list1 == NULL || list2 == NULL ) return NULL ;
	if ( list2->_lengthList == 0 ) { freeList ( list2 ) ; return list1 ; }
	if ( list1->_lengthList == 0 ) { freeList ( list1 ) ; return list2 ; }

	list1->tail->next = list2->head ;
	list2->head->previous = list1->tail ;
	list1->tail = list2->tail ;
	list1->_lengthList += list2->_lengthList ;
	free ( list2 ) ;
	return list1 ;
}

ListIt
listIterator ( List list ) 
{
	ListIt temp ;

	temp = (ListIt)malloc ( sizeof(struct ListIt) ) ;
	if ( list == NULL || temp == NULL ) return NULL ;

	temp->which = list->head ;
	temp->list = list ;
	return temp ;
}

bool
listNext ( ListIt listit , Data data ) 
{
	
	if ( listit == NULL || listit->which == NULL ) {
		free ( listit ) ; return false ;
	}
	listit->list->_copyData ( data , listit->which->dptr ) ;
	listit->which = listit->which->next ;
	return true ;
}

bool 
top ( List list , Data data ) 
{
	return accessHead ( list , data ) ;
}

bool 
pop ( List list , Data data ) 
{
	return deleteHead( list , data ) ;
}

bool 
push ( List list , Data data ) 
{
	return insertHead( list , data ) ;
}

int
lengthList ( List list ) 
{
	if ( list == NULL ) return 0 ;
	return list->_lengthList ;
}


bool
modifyTop ( List list , Data data ) 
{
	if ( list == NULL || list->head == NULL ||
			data == NULL || list->head->dptr == NULL ) 
		return false ;
	list->_copyData ( list->head->dptr , data ) ;
	return true ;
}
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
