/* * * * * * * * * * * * * * * * * * * * * * * * * 
 *
 *	List data type 		Spring 2008
 *
 *      Walt Burkhard		List header file
 *
 * * * * * * * * * * * * * * * * * * * * * * * * */ 

#ifndef LIST_H
#define LIST_H

#include <stdbool.h>

	typedef void * List ; 			
	typedef void * DataL ;
	typedef void * ListIt ;

	List makeList ( int , void * (*copy)() , void (*free)() ) ;	
	void freeList ( List ) ;
	void clearList ( List ) ;

	bool accessList ( List , DataL , int ) ; 	
	int lengthList ( List ) ;	
						
	bool accessHead ( List , DataL ) ; 
	bool insertHead ( List , DataL ) ; 
	bool deleteHead ( List , DataL ) ; 

	bool accessTail ( List , DataL ) ; 
	bool insertTail ( List , DataL ) ; 
	bool deleteTail ( List , DataL ) ;

	List listConcatenate ( List , List ) ;
	bool listNext ( ListIt , DataL ) ;
	ListIt listIterator ( List ) ;

/* 	stack methods		*/

	bool top ( List , DataL ) ; 
	bool pop ( List , DataL ) ; 
	bool push ( List , DataL ) ; 
	bool modifyTop ( List , DataL ) ; 

#endif
