/* = = = = = = = = = = = = = = = = = = = = =
 *
 *	MyData header.		Spring 2008
 *
 * = = = = = = = = = = = = = = = = = = = = = */

typedef struct MyData {
	int zip ;
	char name [21] ;
} * MyData ;

MyData 
makeMyData ( int , char * ) ;

void * 
MyDataCopy ( MyData , MyData ) ;

unsigned
MyDataValue ( MyData ) ;

int 
MyDataDiff ( MyData , MyData ) ;

void 
MyDataPrint ( MyData ) ;

void 
MyDataFree ( MyData ) ;
