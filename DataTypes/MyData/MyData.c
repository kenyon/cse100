/* = = = = = = = = = = = = = = = = = = = = = = = = 
 * 
 *	MyData 			Spring 2008
 *
 * = = = = = = = = = = = = = = = = = = = = = = = = */

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef struct MyData {
	int zip ;
	char name [21] ;
} * MyData ;


MyData 
makeMyData ( int zip , char * name ) 
{

	MyData temp = (MyData) malloc ( sizeof( struct MyData ) ) ;
	temp -> zip = zip ;
	strcpy ( temp->name ,  name ) ;
	return temp ;
}

int 
MyDataDiff ( MyData md , MyData md1 )
{
	return strcmp ( md->name, md1->name ) ;
}

void * 
MyDataCopy ( MyData md , MyData md1 )
{
	if ( md1 == NULL || md == NULL ) return false ;
	return memcpy ( md , md1 , sizeof(struct MyData) ) ;
}

unsigned
MyDataValue ( MyData md )
{
	typedef unsigned long hash_t ;
	enum {crcBits = 32, crcPoly = 0x04c11db7} ;
	enum { crcHiBit=1<<(crcBits-1), crcMask = 0xffffffff } ; 
		// crcMask=((crcHiBit-1)<<1)+1 };
	enum { tblBits=8, tblSize=1<<tblBits } ;
	static hash_t table[tblSize] = {0,0} ;
	enum {byteSize=8,shiftAmt=crcBits-byteSize} ;
	hash_t crcPoly_d = crcPoly ;
	int j , diff , d ;
	unsigned char c ;
	hash_t h = 0 ;

	if ( table[0] == table[1] ) 
		for ( d=0; d<tblBits; ++d ) {
			diff = 1<<d ;
			for ( j = 0 ; j<diff ; ++j )
				table[j+diff] =table[j]^crcPoly_d ;
			crcPoly_d = (crcPoly_d<<1)&crcMask ;
			if ( crcPoly_d & crcHiBit )
				crcPoly_d ^= crcPoly ;
		}
	j = 0 ;
	while ((c = md->name[j++]))
		h = table[(h>>shiftAmt)^c]^((h<<byteSize)&crcMask) ;
	return h ;
}

void 
MyDataPrint ( MyData md ) 
{
	printf ( "name: %-20s  integer: %6d" , md->name , md->zip ) ;
}

void 
MyDataFree ( MyData md )
{
	free ( md ) ;
}
