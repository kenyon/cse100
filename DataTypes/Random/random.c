/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
*
*	pseudo-random numbers. 			Spring 2008
*	
*						Walt Burkhard
*
   = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */

#include <stdlib.h>
#include <time.h>
#include <math.h>

/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
*
*	pseudo-random integers; uniformly distributed in the range
* 				lower to upper.
*
   = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */
int
randomInt ( int lower , int upper )
{
	int k ; 
	double d ;

	d = random()/( (double)RAND_MAX+1 ) ;
	k = floor(d*(upper-lower+1)) ;

	return ( lower + k ) ;
}

/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
*
*	pseudo-random reals; 	uniformly distributed in the range
* 				lower to upper.
*
   = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */

double
randomReal ( double lower , double upper )
{
	double d ;

	d = random()/( (double)RAND_MAX+1 ) ;
	return ( lower + d*(upper-lower) ) ;
}

/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
*
*	initializes pseudo-random sequence generator with the current time
*
   = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */
void
randomize ( void )
{
	srandom((int)2345) ;			// same sequence produced 
	// srandom ( (int)time(NULL) ) ;
}

/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
*
*	initializes pseudo-random sequence generator with a fixed integer
*
   = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */
void
randomizeWith ( int seed )
{
        srandom( seed );
}




