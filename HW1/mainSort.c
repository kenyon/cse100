#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "topSort.h"
#include "List.h"
#include "Int.h"

int
main ( int argc , char * * argv ) 
{
	char iname [100] ;		// input file name.
	FILE * idata , * odata ;
	int n ;
	List sort ;
	Int a ; 
	
	if ( argc > 1 ) 
		strcpy ( iname , argv[1] ) ;
	else {
		fprintf ( stderr , "%s requires an input_file\n" , argv[0] ) ;
		exit (0) ;
	}

	idata = fopen ( iname , "r" ) ;
	
	if ( idata == NULL ) {
		fprintf ( stderr , "trouble opening file %s\n" , iname ) ;
		exit ( 0 ) ;
	}
	odata = fopen ( strcat ( iname , ".sorted" ) , "w+" ) ;
	if ( odata == NULL ) {
		fprintf ( stderr , "trouble opening file %s\n" , iname ) ;
		exit ( 0 ) ;
	}

	n = 0 ;			
	fscanf ( idata , "%d" , &n ) ;
	fseek ( idata , 0 , SEEK_SET ) ;		// set file pointer to first byte again.
	sort = topSort ( idata ) ;
	if ( sort == NULL ) {
		printf ( "there is at least one cycle in the data\n" ) ; exit(0) ;
	} else if ( lengthList(sort) == 0 ) {
			printf ( "there are no items to sort\n" ) ;
			freeList ( sort ) ;
			exit(0) ;
		}

	a = makeInt(0) ;

	while ( deleteTail ( sort , a ) == true ) {
		fprintf ( stderr , "%d " , valueInt(a) ) ;
		fprintf ( odata , "%d " , valueInt(a) ) ;
	}
	fprintf ( stderr , "\n" ) ;
	free ( a ) ;
	freeList ( sort ) ;
	return 0;
}

