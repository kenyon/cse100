/* = = = = = = = = = = = = = = = = = = = = =
 *
 *	Topological Sorting 	Spring 2008
 * 
 *	topSort.c 		Kenyon Ralph
 *	20080418		kralph@ucsd.edu
 *
 * Sorts integers topologically. Input is given by a text file in a simple
 * format.  The first line is the number of elements. The subsequent lines are
 * like this:
 * 1 2
 * 3 4
 *
 * Those lines represent arrows going from 1 to 2 and 3 to 4.
 *
 * Sorted output is returned in a List data type.
 *
 * = = = = = = = = = = = = = = = = = = = = = */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "Int.h"
#include "List.h"

/*----------------------------------------------------------------------------
 *  These functions are really only useful for debugging purposes. They just
 *  show various data. They are pretty self-explanatory if you read the
 *  function names and you understand the topological sorting algorithm.
 *----------------------------------------------------------------------------*/
void show_successor_lists(int, List *);
void show_list(List *);
void show_predecessor_counts(int, int *);

/** Sorts integers topologically.
 *
 * @param[in] idata pointer to a file to read partial ordering info from
 *
 * @return a sorted List
 */
List topSort(FILE * idata)
{
    List sortedlist = makeList(sizeofInt(), copyInt, freeInt);
    bool result = false;

    int i; // only using for loop iterators

    int numrecords = 0; // == the number of arcs in the graph
    fscanf(idata, "%d", &numrecords);
    //printf("numrecords=%d, scanresult=%d\n", numrecords, scanresult);
    //scanresult == -1 on EOF
    if (numrecords == 0) {
	return sortedlist;
    }

    List successor_lists[numrecords];
    //List *successor_lists = (List*)malloc(numrecords * sizeof(List));
//    printf("%d\n", sizeof(successor_lists[0]));
    for (i = 0; i < numrecords; i++) {
	successor_lists[i] = makeList(sizeofInt(), copyInt, freeInt);
	// equivalent:
//	*(successor_lists + i) = makeList(sizeofInt(), copyInt, freeInt);
    }
    //int *predecessor_count = (int*)malloc(numrecords * sizeof(int));
    int predecessor_count[numrecords];
    for (i = 0; i < numrecords; i++) {
	predecessor_count[i] = 0;
    }

    int num = 0;
    int nums[2];

    while (fscanf(idata, "%d", &num) != -1) {
	nums[0] = num;
	fscanf(idata, "%d", &num);
	nums[1] = num;
	//printf("num[0]=%d ", nums[0]);
	//printf("num[1]=%d\n", nums[1]);
	result = insertTail(successor_lists[nums[0]], makeInt(nums[1]));
	if (result == false) {
	    printf("problem inserting Int into a successor_lists list\n");
	    exit(EXIT_FAILURE);
	}
	++predecessor_count[nums[1]];
    }

    //show_predecessor_counts(numrecords, predecessor_count);

    //show_successor_lists(numrecords, successor_lists);

    // build initial no predecessor list
    List no_predecessors = makeList(sizeofInt(), copyInt, freeInt);
    for (i = 0; i < numrecords; i++) {
	if (predecessor_count[i] == 0) {
//	    printf("predecessor_count[%d]=%d\n", i, predecessor_count[i]);
	    result = insertHead(no_predecessors, makeInt(i));
//	    printf("result=%d\n", result);
	    if (result == false) {
		printf("problem inserting Int into no_predecessors list\n");
		exit(EXIT_FAILURE);
	    }
	}
    }

//    show_list(no_predecessors);

//    show_predecessor_counts(numrecords, predecessor_count);

//    printf("starting sort.\n");

    // finally, the meaty portion of the sorting algorithm
    while (lengthList(no_predecessors) > 0) {
	Int no_predecessor_item = makeInt(0);
	pop(no_predecessors, no_predecessor_item);
//	printf("%d ", valueInt(item));
	while (lengthList(successor_lists[valueInt(no_predecessor_item)]) > 0) {
	    Int successor_item = makeInt(0);
	    pop(successor_lists[valueInt(no_predecessor_item)], successor_item);
	    --predecessor_count[valueInt(successor_item)];
	    if (predecessor_count[valueInt(successor_item)] == 0) {
		push(no_predecessors, successor_item);
	    }
	    freeInt(successor_item);
	}
	//show_list(sortedlist);
	push(sortedlist, no_predecessor_item);
	freeInt(no_predecessor_item);
    }

    // in this case there is a cycle in the graph
    if (lengthList(sortedlist) != numrecords) {
	return NULL;
    }

//    show_predecessor_counts(numrecords, predecessor_count);

    // free stuff
    freeList(no_predecessors);
    for (i = 0; i < numrecords; i++) {
	freeList(successor_lists[i]);
    }

//    printf("done in topSort.c\n");

    return sortedlist;
}

void show_successor_lists(int numrecords, List *successor_lists)
{
    int i;
    for (i = 0; i < numrecords; i++) {
	printf("successor_lists[%d]: ", i);
	ListIt li = listIterator(successor_lists[i]);
	Int item = makeInt(0);
	while (listNext(li, item)) {
	    printf("%d ", valueInt(item));
	}
	printf("(%d)", lengthList(successor_lists[i]));
	printf("\n");
	freeInt(item);
	free(li);
    }
    return;
}

void show_predecessor_counts(int numrecords, int *predecessor_count)
{
    int i;
    for (i = 0; i < numrecords; i++) {
	printf("predecessor_count[%d]=%d\n", i, predecessor_count[i]);
    }
    return;
}

void show_list(List *no_predecessors)
{
    printf("showing list: ");
    ListIt li = listIterator(no_predecessors);
    Int item = makeInt(0);
    while (listNext(li, item)) {
	printf("%d ", valueInt(item));
    }
    printf("(%d)", lengthList(no_predecessors));
    printf("\n");
    freeInt(item);
    free(li);
    return;
}
