/** @file
 * Binary tree hash table implementation.
 *
 * @author Kenyon Ralph <kralph@ucsd.edu>
 * @date due 20080516
 */

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define NDEBUG

#include "List.h"
#include "TreeNode.h"
#include "Queue.h"

#ifndef DATAT
#define DATAT
typedef void *DataT;
#endif

/*
 * Structures.
 */

/** Holds table search performance data.
 */
typedef struct {
    unsigned successfulNumerator;
    unsigned successfulDenominator;
    unsigned unsuccessfulNumerator;
    unsigned unsuccessfulDenominator;
} Perform;

/** The binary tree hash table with passbits.
 */
typedef struct Table {
    /** An array of pointers to DataT objects.
     */
    DataT *slots;

    /** A bool array indicating whether the corresponding slot is full.
     */
    bool *isFull;

    /** Array whose elements are single passbits (actually bools in this
     * implementation) corresponding to the data slots.
     */
    bool *passbits;

    /** A TreeNode queue for doing the implicit binary tree activity during
     * insertTable().
     */
    Queue queue;

    /** Tracks the number of probes required to reach the records currently in
     * the table. This is for successful searches.
     */
    int sProbes;

    /** The number of slots in the table. Must be a prime number. This value is
     * automatically assigned to the next larger prime number (by nextprime())
     * if a nonprime is given in makeTable().
     */
    size_t sizeTable;

    /** The number of bytes per data object.
     */
    size_t sizeData;

    /** A count of the number of data items actually in the table (as opposed
     * to number of slots, which is Table.sizeTable).
     */
    unsigned int count;

    /** Pointer to a function that gives the difference between two data
     * objects.
     */
    int (*diffData)();

    /** Pointer to a function that gives the hash of a data object.
     */
    unsigned (*hashData)();

    /** Pointer to a function that copies data objects.
     */
    void *(*copyData)();

    /** Pointer to a function that frees memory allocated by data objects.
     */
    void (*freeData)();
} *Table;

/*
 * Some function prototypes. Only functions that are used out of definition
 * order are prototyped here.
 */

void clearTable(Table);
unsigned nextprime(unsigned int);
int testLoc(Table, DataT);
void show_TreeNode_queue(Queue);
int startof(Table, DataT);
int stepof(Table, DataT);
DataT get_slot_data(Table, unsigned int);

/*
 * Functions.
 */

/** Makes a Table. Initializes/clears all fields. The array of data values must
 * contain a prime number of slots. Each slot has an associated passbit.  The
 * table is initialized in its empty state.
 *
 * @param[in] sizeTable number of slots in the table
 * @param[in] sizeData size, in bytes, of the data items to be stored
 * @param[in] hash pointer to a function which maps a record key to an unsigned
 * int value
 * @param[in] diff pointer to a function which determines whether two data
 * objects have equal keys
 * @param[in] copy pointer to a function which copies one data object to
 * another
 * @param[in] free pointer to a function which returns a record instance to the
 * heap
 *
 * @return the Table that was made, or NULL if a malloc or calloc returns NULL
 *
 * Diff and hash depend only on record keys.
 */
Table makeTable(size_t sizeTable, size_t sizeData, int  (*diff)(),
	unsigned int (*hash)(), void *(*copy)(), void (*free)())
{
    //printf(">Starting makeTable.\n");

    //int sizeTable_orig = sizeTable;
    if (sizeTable > 3) { //nextprime doesn't recognize < 3 as primes
	sizeTable = nextprime(sizeTable);
    }

    Table newTable = (Table)malloc(sizeof(struct Table));
    if (newTable == NULL) {
	return NULL;
    }
    //printf("malloc'd %lu bytes for a struct Table.\n",
	    //(long unsigned int)sizeof(struct Table));

    newTable->slots = (DataT*)calloc(sizeTable, sizeof(DataT));
    if (newTable->slots == NULL) {
	return NULL;
    }
    /*
    printf("calloc'd for %d slots, %lu bytes per slot = %lu bytes total.\n",
	    sizeTable, (long unsigned int)sizeof(DataT),
	    (long unsigned int)(sizeTable * sizeof(DataT)));
	    */

    newTable->isFull = (bool*)calloc(sizeTable, sizeof(bool));
    if (newTable->isFull == NULL) {
	return NULL;
    }
    //printf("calloc'd for %d isFull elements, %lu bytes per slot = %lu bytes total.\n",
	    //sizeTable, (long unsigned int)sizeof(bool),
	    //(long unsigned int)(sizeTable * sizeof(bool)));

    newTable->passbits = (bool*)calloc(sizeTable, sizeof(bool));
    if (newTable->passbits == NULL) {
	return NULL;
    }
    //printf("calloc'd for %d passbits elements, %lu bytes per slot = %lu bytes total.\n",
	    //sizeTable, (long unsigned int)sizeof(bool),
	    //(long unsigned int)(sizeTable * sizeof(bool)));

    newTable->queue = make_Queue(sizeof(TreeNode), copy_TreeNode, free_TreeNode);
    //printf("made TreeNode queue with sizeData=%lu bytes.\n",
	    //(long unsigned int)sizeof(TreeNode));

    newTable->count = 0;
    //printf("set newTable->count=%d.\n", newTable->count);

    newTable->sProbes = 0;
    //printf("set newTable->sProbes=%d.\n", newTable->sProbes);

    newTable->sizeTable = sizeTable;
    //printf("set newTable->sizeTable=%d.\n", newTable->sizeTable);

    newTable->sizeData = sizeData;
    //printf("set newTable->sizeData=%d.\n", newTable->sizeData);

    newTable->diffData = diff;
    //printf("set newTable->diffData=%p (diff=%p).\n", newTable->diffData, diff);

    newTable->hashData = hash;
    //printf("set newTable->hashData=%p (hash=%p).\n", newTable->hashData, hash);

    newTable->copyData = copy;
    //printf("set newTable->copyData=%p (copy=%p).\n", newTable->copyData, copy);

    newTable->freeData = free;
    //printf("set newTable->freeData=%p (free=%p).\n", newTable->freeData, free);

    //printf(">Done with makeTable.\n");
    return newTable;
}

/** Destroys the table and returns it to the heap.
 *
 * @param[in,out] table the address of a table to destroy
 */
void freeTable(Table *table)
{
    //printf(">Starting freeTable.\n");

    int i;
    for (i = 0; i < (*table)->sizeTable; i++ ) {
	(*table)->freeData((*table)->slots[i]);
	//printf("freed slot %d.\n", i);
    }
    free((*table)->slots);
    //printf("freed slots.\n");

    free((*table)->isFull);
    //printf("freed isFull.\n");

    free((*table)->passbits);
    //printf("freed passbits.\n");

    free_Queue((*table)->queue);
    //printf("freed queue.\n");

    free(*table);
    table = NULL;

    //printf(">Done with freeTable.\n");
    return;
}

/** Resets the table to its initial empty state, as if it had just been made
 * with makeTable().
 *
 * @param[in,out] table the table to clear
 */
void clearTable(Table table)
{
    int i;
    for (i = 0; i < table->sizeTable; i++ ) {
	table->freeData(table->slots[i]);
	table->isFull[i] = false;
	table->passbits[i] = false;
    }
    free(table->slots);
    table->slots = (DataT*)calloc(table->sizeTable, sizeof(DataT));
    if (table->slots == NULL) {
	printf("memory allocation for table->slots failed in clearTable().\n");
	exit(EXIT_FAILURE);
    }

    clear_Queue(table->queue);

    table->count = 0;
    table->sProbes = 0;

    return;
}

/** Copies the data parameter into the table using a binary tree hashing
 * algorithm, with passbits.
 *
 * @param[in] table the table to look at
 * @param[in] data the data item to insert
 *
 * @return true if successful; false if a duplicate is found, if insufficient
 * heap space is available, if the table is full, or if other problems occur.
 */
bool insertTable(Table table, DataT data)
{
    if (table->count == table->sizeTable) {
	//printf("table is full, cannot insert!\n");
	return false;
    }

    clear_Queue(table->queue);

    bool done = false;
    unsigned int index, start, step;
    index = start = startof(table, data);
    step = stepof(table, data);
    unsigned int tree_location = 1, x, y;
    //printf(">Inserting data with initial index=%u, step=%u, start=%u.\n", index, step, start);
    //printf("showing queue:\n");
    //show_TreeNode_queue(table->queue);

    enqueue(table->queue, make_TreeNode(start, step, tree_location));
    //printf(">> enqueued (%d, %d, %d).\n", start, step, tree_location);

    while (done == false) {
	TreeNode tn = make_TreeNode(0, 0, 0);

	dequeue(table->queue, tn);

	x = get_table_location(tn);
	//printf("x=%d, ", x);
	y = get_step(tn);
	//printf("y=%d, ", y);
	tree_location = get_tree_location(tn);
	//printf("tree_location=%d.\n", tree_location);

	//printf("<< dequeued (%d, %d, %d).\n", x, y, tree_location);

	if(!table->isFull[x]) {
	    //printf("****table slot x=%d is not full.\n", x);
	    done = true;
	} else {
	    //printf("****table slot x=%d is full.\n", x);

	    // see if slots[x] is a duplicate of data
	    if (!table->diffData(data, table->slots[x])) {
		//printf("tried to insert a duplicate.\n");
		return false;
	    }

	    // node z left edge:
	    enqueue(table->queue, make_TreeNode((x + y) % table->sizeTable, y, 2 * tree_location));
	    //printf(">> enqueued (%d, %d, %d) (left edge).\n", (x + y) % table->sizeTable, y, 2 * tree_location);

	    // node z right edge:
	    enqueue(table->queue,
		    make_TreeNode((x + stepof(table, get_slot_data(table, x))) % table->sizeTable,
			stepof(table, get_slot_data(table, x)),
			2 * tree_location + 1));
	    /*
	    printf(">> enqueued (%d, %d, %d) (right edge).\n",
		    (x + stepof(table, get_slot_data(table, x))) % table->sizeTable,
		    stepof(table, get_slot_data(table, x)),
		    2 * tree_location + 1);
		    */
	}

	free_TreeNode(tn);
    }

    //printf("after phase 1: x=%d, y=%d, tree_location=%d.\n", x, y, tree_location);
    //printf("showing queue:\n");
    //show_TreeNode_queue(table->queue);
    //printf("\n");

    //phase two starts here: path to root determination and reordering

    int path[50]; //FIXME: see if we can get rid of this magic number
    int i;
    for (i = 0; i < 50; i++) {
	path[i] = 0;
    }

    /*
    for (i = 0; i < 50; i++) {
	printf("path[%d]=%d, ", i, path[i]);
    }
    printf("\n");
    */

    // determine path from node tree_location to root.
    for (i = 0; tree_location != 1 && i < 50; tree_location /= 2, i++) {
	path[i] = tree_location;
	//printf("path[%d]=%d.\n", i, path[i]);
    }
    path[i] = 1;
    //printf("path[%d]=%d.\n", i, path[i]);
    table->sProbes = table->sProbes + i + 1;

    DataT datacopy = (DataT)malloc(table->sizeData);
    table->copyData(datacopy, data);
    DataT slotsindexcopy = (DataT)malloc(table->sizeData);

    int j;
    //int booger; //another loop incrementor used for debugging loops
    for (j = path[--i]; index != x; j = path[--i]) {
	assert(i >= 0);
	//printf("i=%d, j=%d (j should = path[i]).\n", i, j);
	if (j % 2 == 1) {
	    step = stepof(table, get_slot_data(table, index));
	    //printf("data=%d, step=%d.\n", valueInt((Int)get_slot_data(table, index)), step);
	    //table->passbits[index] = true;
	    //printf("set passbits[%d]=%d.\n", index, table->passbits[index]);

	    /*
	    printf("showing table.\n");
	    for (booger = 0; booger < table->sizeTable; booger++) {
		if (get_slot_data(table, booger) != NULL) {
		    printf("table slot %d: %d (%p).\n", booger, valueInt((Int)get_slot_data(table, booger)), get_slot_data(table, booger));
		} else {
		    printf("table slot %d: NULL (%p).\n", booger, get_slot_data(table, booger));
		}
	    }
	    */

	    //printf("swapping datacopy and slots[index].\n");
	    table->copyData(slotsindexcopy, table->slots[index]);
	    table->copyData(table->slots[index], datacopy);
	    table->copyData(datacopy, slotsindexcopy);

	    /*
	    printf("showing table.\n");
	    for (booger = 0; booger < table->sizeTable; booger++) {
		if (get_slot_data(table, booger) != NULL) {
		    printf("table slot %d: %d (%p).\n", booger, valueInt((Int)get_slot_data(table, booger)), get_slot_data(table, booger));
		} else {
		    printf("table slot %d: NULL (%p).\n", booger, get_slot_data(table, booger));
		}
	    }
	    */
	}

	table->passbits[index] = true;
	index = (index + step) % table->sizeTable;
	//printf("index=%d, step=%d.\n", index, step);
    }

    assert(table->slots[x] == NULL);

    table->slots[x] = (DataT)malloc(table->sizeData);

    if (table->slots[x] == NULL) {
	return false;
    }

    table->copyData(table->slots[x], datacopy);
    table->isFull[x] = true;
    //printf("copied datacopy to index=%d.\n", x);

    /*
    printf("showing table.\n");
    for (booger = 0; booger < table->sizeTable; booger++) {
	if (get_slot_data(table, booger) != NULL) {
	    printf("table slot %d: %d (%p).\n", booger, valueInt((Int)get_slot_data(table, booger)), get_slot_data(table, booger));
	} else {
	    printf("table slot %d: NULL (%p).\n", booger, get_slot_data(table, booger));
	}
    }
    */

    table->count++;

    free(datacopy);
    free(slotsindexcopy);
    //printf(">Done with insertTable.\n\n");
    return true;
}

/** Returns the start of the data given. Calculated using the formula start =
 * Table.hashData() % Table.sizeTable.
 *
 * @param[in] table the table to look at
 * @param[in] data the data item to use in the calculation
 *
 * @return the start value of the data item
 */
int startof(Table table, DataT data)
{
    return table->hashData(data) % table->sizeTable;
}

/** Returns the step of the given data item. Calculated using the formula step
 * = Table.hashData() % (Table.sizeTable - 1) + 1.
 *
 * @param[in] table the table to look at
 * @param[in] data the data item to use in the calculation
 *
 * @return the step of the data item
 */
int stepof(Table table, DataT data)
{
    return table->hashData(data) % (table->sizeTable - 1) + 1;
}

/** Returns the data item in the specified slot.
 *
 * @param[in] table the table to look at
 * @param[in] slot the slot to get a data item from
 *
 * @return the data item in the specified slot. Possibly NULL if that's what is
 * in the slot.
 */
DataT get_slot_data(Table table, unsigned int slot)
{
    return table->slots[slot];
}

/** Returns the value of Table.isFull for the given slot.
 *
 * @param[in] slot the Table.slots to look at
 * @param[in] table the table to look at
 *
 * @return the value of Table.isFull for the given slot
 */
bool get_slot_isFull(Table table, unsigned int slot)
{
    return table->isFull[slot];
}

/** Returns the value of Table.passbits for the given slot.
 *
 * @param[in] slot the Table.slots to look at
 * @param[in] table the table to look at
 *
 * @return the value of Table.passbits for the given slot
 */
bool get_slot_passbit(Table table, unsigned int slot)
{
    return table->passbits[slot];
}

/** Attempts to locate the record within the table with key matching the key of
 * its data parameter. Copies the record from the table into the record
 * referenced by the parameter if found.
 *
 * @param[in] table the table to look at
 * @param[in,out] data the data item to access
 *
 * @return true if the record is found, false otherwise.
 */
bool accessTable(Table table, DataT data)
{
    if (testLoc(table, data) >= 0) {
	table->copyData(data, table->slots[testLoc(table, data)]);
    }
    return testLoc(table, data) >= 0;
}

/** Measures successful and unsuccessful search lengths.
 *
 * The successful search length is determined for all the records within the
 * table.
 *
 * The unsuccessful search length is determined for all possible varieties of
 * unsuccessful searches within the table. For a table with n slots, there are
 * n*(n-1) such searches. That is, the calculation assumes an unsuccessful
 * search can begin in any location within the table and have any acceptable
 * step size; moreover, these searches are equally likely.
 *
 * The returned Perform structure contains four values with self-explanatory
 * purposes: successfulNumerator, successfulDenominator, unsuccessfulNumerator,
 * unsuccessfulDenominator. We already know successfulDenominator is m, the
 * number of records stored, and UnsuccessfulDenominator is n*(n-1).
 *
 * @param[in] table the table to look at
 *
 * @return a Perform structure with the measurements
 */
Perform performTable(Table table)
{
    Perform p;

    p.unsuccessfulNumerator = 0;
    p.unsuccessfulDenominator = table->sizeTable * (table->sizeTable - 1);

    p.successfulNumerator = table->sProbes;
    p.successfulDenominator = table->count;

    int index, start, step;

    for (start = 0; start < table->sizeTable; start++) {
	for (step = 1; step < table->sizeTable; step++) {
	    index = start;
	    p.unsuccessfulNumerator++;
	    while (table->passbits[index]) {
		p.unsuccessfulNumerator++;
		index = (index + step) % table->sizeTable;
	    }
	}
    }

    return p;
}

/** Returns the index of the slot containing the specified data item.
 * Otherwise, returns -(index+1) for the index of the first terminating slot
 * encountered.
 *
 * A slot with a false passbit associated with the search is a terminating
 * slot.
 *
 * @param[in] table the table to look at
 * @param[in,out] data the data item to look for. Copies the record from the table
 * into data if found.
 *
 * @return the index of the slot containing the specified data item
 */
int testLoc(Table table, DataT data)
{
    unsigned int index, start, step;

    index = start = table->hashData(data) % table->sizeTable;
    step = table->hashData(data) % (table->sizeTable - 1) + 1;

    while (table->passbits[index] == true &&
	    (table->diffData(table->slots[index], data) != 0)) {
	index = (index + step) % table->sizeTable;
    }

    if (table->isFull[index] == true &&
	    (table->diffData(table->slots[index], data) == 0)) {
	table->copyData(data, table->slots[index]);
	//printf(">>testLoc index=%d.\n", index);
	return index;
    } else {
	//printf(">>testLoc index=%d.\n", -(index + 1));
	return -(index + 1);
    }
}

/** Returns the value of the passbit at the given index.
 *
 * @param[in] table the table to look at
 * @param[in] index the index to look at
 *
 * @return the value of the passbit at the given index
 */
bool testPB(Table table, unsigned int index)
{
    return table->passbits[index];
}

/** Gives the first prime number greater than the given number. From Professor
 * Burkhard's nextprime.c.
 *
 * @param[in] s number from which to start looking for a prime number
 *
 * @return the first prime number greater than s
 */
unsigned nextprime(unsigned int s)
{
    unsigned mysize, j, k; 

    if (s % 2 == 0)
	s++;
    for (mysize = s; ; mysize += 2) {
	j = (unsigned)pow((double)mysize, 0.5);
	for (k = 3; k <= j; k += 2)
	    if (mysize % k == 0) break; // not a prime
	if (mysize % k == 0)
	    continue;
	else
	    return mysize;
    }
}

/** Returns the size of the table.
 *
 * @param[in] table the table to look at
 *
 * @return the size of the table
 */
size_t get_tablesize(Table table)
{
    return table->sizeTable;
}

void show_TreeNode_queue(Queue q)
{
    ListIt li = listIterator(q);
    TreeNode tn = make_TreeNode(0, 0, 0);
    int i;
    for (i = 0; listNext(li, tn); i++) {
	printf("%d: ", i); print_TreeNode(tn); printf("\n");
    }
    printf("Queue length: %d.\n", lengthList(q));
    //FIXME: WTF? Uncommenting the below lines causes segfault; I'm ignoring it
    //for now.
    //free_TreeNode(tn);
    //free(li);
}

/* vim: set si ai noet sts=1 tw=0 sw=4: */
