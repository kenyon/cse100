/* = = = = = = = = = = = = = = = = = =
 *
 *	creates a performance file  
 *
 * = = = = = = = = = = = = = = = = = = */
#include <stdio.h>
#include <stdlib.h>
#include "Table.h"
#include "Int.h"

int 
main ( int argc , char * argv[] ) 
{
	Table table ;     
	Int a ; 
	Perform results ; 
	double successful , unsuccessful ;
	int i , j , load , size , number ;
	FILE * BinHashSS ;
	char name[100] ; 
	srandom( 0 ) ;

	if ( argc == 3 ) {
		size = atoi(argv[1]) ;
		number = atoi(argv[2]) ;
	} else {
		printf ( "%s size number\n" , argv[0] ) ;
		exit(0) ;
	}

	sprintf ( name , "BinHash.%d.%d" , size , number  ) ;
        BinHashSS = fopen ( name , "w+" ) ;
        fprintf ( BinHashSS , "%f\t%f\t%f\tBinHash.%d.%d\n" , 
							0.0, 1.0, 1.0, size, number ) ;
        fflush ( BinHashSS ) ;

	table = makeTable ( size , sizeofInt() , diffInt , hashInt , copyInt , freeInt ) ;

	a = makeInt ( 0 ) ;
	for ( load = 1 ; load <= size ; load ++ ) {
		successful = 0 ; unsuccessful = 0 ;
		for (i=1; i<=number; i++ ) {
			clearTable ( table ) ;
			for ( j = 0 ; j < load ;  j++ ) {
				updateInt ( a , random() ) ;
				insertTable ( table , a ) ;
			}
			results = performTable ( table ) ;
			successful += (double)results.successfulNumerator/results.successfulDenominator;
			unsuccessful += (double)results.unsuccessfulNumerator/results.unsuccessfulDenominator ;
		}
		fprintf ( BinHashSS , "%f\t%f\t%f\tBinHash.%d.%d\n" , (double)load/size , 
			successful/number , 
			unsuccessful/number , size , number ) ;
		fflush ( BinHashSS ) ;
	}
	freeTable ( &table ) ;
	freeInt ( a ) ;
	return EXIT_SUCCESS;
}
