/** @file
 */

#ifndef QUEUE_H
#define QUEUE_H

#include "List.h"

typedef List Queue;
typedef void *DataQ;

Queue make_Queue(size_t, void *(*copyData)(), void (*freeData)());
void free_Queue(Queue);
bool enqueue(Queue, DataQ);
bool dequeue(Queue, DataQ);
void clear_Queue(Queue);

#endif
