#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "Table.h"
#include "MyData.h" 

struct MyData myData [] = { {92014,"peg smith"} , {92093,"herb jones"} , {24680,"diane lake"} ,
		{12345, "willie yuu"}, {23455,"darrell cruz"} , {34567,"jehan roma"} , 
		{45678,"patti bliss"}, {56789,"alex kronkov"} , {67890,"beth smith"} ,
		{78901,"alan knox"} , {89012, "khang tran"} , {90123,"ray bass"} , 
		{23471,"steve finny"} , {23423, "lee park"} , {711711,"phil bose"} } ;

void 					// calls    testLoc   performTable testPB		
printTable ( Table table ) 		
{
	int j , i ; 
	Perform result ;
	unsigned step , val , index , n ;
	int location[15] ;

	for ( j = 0 ; j < 15 ; j ++ ) 
		location[j] = testLoc( table, myData+j ) ;	// directly accessed.
	
	n = 13 ;					// another function within Table !!?!
	for ( i = 0 ; i < n ; i++ ) {
		printf ( "%3d: " , i ) ;
		printf ( "%c " , testPB(table,i)?'t':'f' ) ;
		for ( j = 0 ; j < n && location[j] != i ; j++ ) ;	// determine which record is in slot i
		if ( j < n ) {	
			MyDataPrint ( myData+j ) ;	// should/could be part of Table structure.
			printf ( " | " ) ;
		 	val =  MyDataValue ( myData+j ) ;	
			index = val % n ;
			step =  val % ( n - 1 ) + 1 ;
			while ( index != i ) {
				printf ( "%d, " , index ) ;
				index += step ;
				if ( index >= n ) 
					index -= n ;
			}
			printf ( "%d" , index ) ;
		}
		printf ( "\n" ) ;
	}
	result = performTable ( table ) ;
	printf ( "successful probe count %d\t\t\tunsuccessful probe count %d\n" , 
		result.successfulNumerator, result.unsuccessfulNumerator ) ;
	printf ( "average successful search length %f\tunsuccessful search length %f\n" , 
		(double)result.successfulNumerator/result.successfulDenominator ,
		(double)result.unsuccessfulNumerator/result.unsuccessfulDenominator ) ;
}

int 
main ( void ) 
{
	Table table ;     
	int j;

	table = makeTable ( 13 , sizeof(struct MyData) , 
		 MyDataDiff , MyDataValue , MyDataCopy , MyDataFree )  ;

	for ( j = 0 ; j < 10 ;  j++ ) 
		insertTable ( table , myData + j ) ;

	printTable ( table ) ;

	freeTable ( &table ) ;
	return EXIT_SUCCESS;
}   
