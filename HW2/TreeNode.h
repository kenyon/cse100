/** @file
 */

#ifndef TREENODE_H
#define TREENODE_H

typedef void *TreeNode;

TreeNode make_TreeNode(unsigned int, unsigned int, unsigned int);
void *copy_TreeNode(TreeNode, TreeNode);
void free_TreeNode(TreeNode);
void print_TreeNode(TreeNode);
void set_table_location(TreeNode, unsigned int);
void set_step(TreeNode, unsigned int);
void set_tree_location(TreeNode, unsigned int);
int get_table_location(TreeNode);
int get_step(TreeNode);
int get_tree_location(TreeNode);

#endif
