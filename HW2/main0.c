/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
 *
 *	This is the passbit example from the notes.
 *
 * = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Table.h"
#include "Int.h"

#define Number 5
#define TableSize 5

static
int x[Number] = { 10 , 7 , 1 , 8 , 0 } ;
char * name[] = { "cow" , "pig" , "dog" , "ape" , "yak" } ;

void
printT ( Table table ) 		// calls testPB  testLoc   performTable
{
	int j , i ; 
	Int a ;
	Perform result ;
	unsigned location[Number] , step , val , index ;

	a = makeInt(0) ;
	for ( j = 0 ; j < Number ; j ++ ) {
		updateInt ( a , x[j] ) ;
		location[j] = testLoc( table, a ) ;
	}
	for ( i = 0 ; i < TableSize ; i++ ) {
		printf ( "%3d: " , i ) ;
		printf ( "%c " , (testPB(table, i )==true)?'t':'f' ) ;
		for ( j = 0 ; j < Number && location[j] != i ; j++ ) ; 	// determines which record is in slot i
		if ( j < Number ) {	
			// printf ( "%8d | " , x[j] ) ;
			printf ( "%8s | " , name[j] ) ;
		 	val =  x[j] ;
			index = val % TableSize ;
			step =  val % ( TableSize - 1 ) + 1 ;
			while ( index != i ) {
				printf ( "%d, " , index ) ;
				index += step ;
				if ( index >= TableSize ) 
					index -= TableSize ;
			}
			printf ( "%d" , index ) ;
		}
		printf ( "\n" ) ;
	}
	result = performTable ( table ) ;
	printf ( "successful probe count %d\t\t\tunsuccessful probe count %d\n" , 
		result.successfulNumerator, result.unsuccessfulNumerator ) ;
	printf ( "average successful search length %f\tunsuccessful search length %f\n" , 
		(double)result.successfulNumerator/result.successfulDenominator ,
		(double)result.unsuccessfulNumerator/result.unsuccessfulDenominator ) ;
}

int
main ( void )
{
	Table table ;
	Int a ;
	int j ;

	table = makeTable ( TableSize, sizeofInt(), diffInt, hashInt, copyInt , freeInt );
	a = makeInt ( 0 ) ;
	for ( j = 0 ; j < Number ;  j++ ) {
			updateInt ( a , x[j] ) ;
			insertTable ( table , a ) ;
	}
	printf ( "\nThe example within Binary Tree Hashing notes; same as in lecture but with different keys.\n\n" ) ;
	printT ( table ) ;  
	freeTable ( &table ) ;
	freeInt ( a ) ;
	return EXIT_SUCCESS;
}
