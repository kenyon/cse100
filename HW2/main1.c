#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "Table.h"
#include "Int.h"

#define Number 8
#define TableSize 11

static
int x[Number] = { 1234 , 2345 , 2342 , 6781 , 234 , 9982 , 235 , 3589 } ;

void
printT ( Table table ) 		// calls testPB, testLoc, & perform
{
	int j , i ; 
	Int a ;
	Perform result ;
	unsigned location[Number] , step , val , index ;

	a = makeInt(0) ;
	for ( j = 0 ; j < Number ; j ++ ) {
		updateInt ( a , x[j] ) ;
		location[j] = testLoc( table, a ) ;
	}
	for ( i = 0 ; i < TableSize ; i++ ) {
		printf ( "%3d: " , i ) ;
		printf ( "%c " , testPB(table,i)?'t':'f' ) ;
		for ( j = 0 ; j < Number && location[j] != i ; j++ ) ;
		if ( j < Number ) {	
			printf ( "%8d | " , x[j] ) ;
		 	val =  x[j] ;
			index = val % TableSize ;
			step =  val % ( TableSize - 1 ) + 1 ;
			while ( index != i ) {
				printf ( "%d, " , index ) ;
				index += step ;
				if ( index >= TableSize ) 
					index -= TableSize ;
			}
			printf ( "%d" , index ) ;
		}
		printf ( "\n" ) ;
	}
	result = performTable ( table ) ;
	printf ( "successful probe count %d\t\t\tunsuccessful probe count %d\n" , 
		result.successfulNumerator, result.unsuccessfulNumerator ) ;
	printf ( "average successful search length %f\tunsuccessful search length %f\n" , 
		(double)result.successfulNumerator/result.successfulDenominator, 
		(double)result.unsuccessfulNumerator/result.unsuccessfulDenominator ) ;
}

int 
main ( void ) 
{
	Table table ;     
	Int a ; 
	int j;

	table = makeTable ( TableSize, sizeofInt(), diffInt , 
				hashInt, copyInt , freeInt ) ;

	a = makeInt ( 0 ) ;
	for ( j = 0 ; j < Number ;  j++ ) {
			updateInt ( a , x[j] ) ;
			insertTable ( table , a ) ;
	}
	printT ( table ) ;  
	
	updateInt ( a , x[3] ) ;
	if ( accessTable( table , a ) == true ) 
		printf ( "record %d is present in location %d\n" , x[3] , testLoc(table,a) ) ;
	else	printf ( "the table is incorrect\n" ) ;
	updateInt ( a , 7766 ) ;
	if ( accessTable( table , a ) == false)
		printf ( "record %d is not present: testLoc returns %d\n" , 7766 , 
					testLoc(table,a) ) ;
	else	printf ( "the table is incorrect\n" ) ;
	updateInt ( a , 90 ) ;
	if ( accessTable( table , a ) == false)
		printf ( "record %d is not present: testLoc returns %d\n" , 90 , 
					testLoc(table,a) ) ;
	else	printf ( "the table is incorrect\n" ) ;
	updateInt ( a , x[3] ) ;
	if ( insertTable ( table , a ) == false ) 
		printf ( "something weird happened -- duplicate insertion %d attempted or heap space depleted\n" , x[3] ) ;

	freeTable ( &table ) ;
	freeInt(a) ;
	return EXIT_SUCCESS;
}
