/** @file
 * Program to test my table implementation as I develop it. Based on Professor
 * Burkhard's main0.c.
 *
 * @author Kenyon Ralph
 */

#include <stdio.h>
#include <stdlib.h>

#include "Int.h"
#include "Table.h"
#include "TreeNode.h"
#include "Queue.h"

/*
 * Some function prototypes.
 */

//void show_TreeNode_queue(Queue);

/** Number of data items.
 */
#define Number 5

/** Number of table slots.
 */
#define TableSize 5

static int x[Number] = {10, 7, 1, 8, 0};
char *name[] = { "cow", "pig", "dog", "ape", "yak"};

/** Prints a Table and performance data. Calls testPB, testLoc, and
 * performTable.
 *
 * @param[in] table the table to print.
 */
void printT(Table table)
{
    int j, i;
    Int a;
    Perform result;
    unsigned location[Number], step, val, index;

    a = makeInt(0);

    for (j = 0; j < Number; j++) {
	updateInt(a, x[j]);
	location[j] = testLoc(table, a);
    }

    for (i = 0; i < TableSize; i++) {
	printf("%3d: ", i);
	printf("%c ", (testPB(table, i ) == true) ? 't' : 'f');
	for (j = 0; j < Number && location[j] != i; j++); // determines which record is in slot i
	if (j < Number) {
	    // printf("%8d | ", x[j]);
	    printf("%8s | ", name[j]);
	    val = x[j];
	    index = val % TableSize;
	    step = val % (TableSize - 1) + 1;
	    while (index != i) {
		printf("%d, ", index);
		index += step;
		if (index >= TableSize) {
		    index -= TableSize;
		}
	    }
	    printf("%d", index);
	}
	printf("\n");
    }
    result = performTable(table);

    printf("successful probe count %d\t\t\tunsuccessful probe count %d\n",
	    result.successfulNumerator, result.unsuccessfulNumerator);

    printf("average successful search length %f\tunsuccessful search length %f\n",
	    (double)result.successfulNumerator/result.successfulDenominator,
	    (double)result.unsuccessfulNumerator/result.unsuccessfulDenominator);
}

int main()
{
    Table table;
    const size_t tablesize = 5;

    //table = makeTable(TableSize, sizeofInt(), diffInt, hashInt, copyInt, freeInt);
    //while (1) { //for memory leak checking
    table = makeTable(tablesize, sizeofInt(), diffInt, hashInt, copyInt, freeInt);

    int i;
    for (i = 0; i < get_tablesize(table); i++) {
	Int intobj;
	intobj = makeInt(random() % 1000);
	printf("intobj val=%d\n", valueInt(intobj));
	insertTable(table, intobj);
	freeInt(intobj);
    }

    printf("Showing table.\n");
    for (i = 0; i < get_tablesize(table); i++) {
	//printf("slot data=%p.\n", get_slot_data(table, i));
	if (get_slot_data(table, i) != NULL) {
	    printf("table slot %d: %d (%p)\n", i, valueInt((Int)get_slot_data(table, i)), get_slot_data(table, i));
	}
    }

    printf("Showing isFull.\n");
    for (i = 0; i < get_tablesize(table); i++) {
	//somecondition ? doiftrue : doiffalse;
	get_slot_isFull(table, i) ?
	    printf("isFull[%d]: true.\n", i) : printf("isFull[%d]: false.\n", i);
    }

    printf("Showing passbits.\n");
    for (i = 0; i < get_tablesize(table); i++) {
	//somecondition ? doiftrue : doiffalse;
	get_slot_passbit(table, i) ?
	    printf("passbits[%d]: true.\n", i) : printf("passbits[%d]: false.\n", i);
    }

    printf("\ntest inserting a duplicate.\n\n");
    Int adupe = makeInt(383);
    insertTable(table, adupe);
    freeInt(adupe);

    printf("Showing table.\n");
    for (i = 0; i < get_tablesize(table); i++) {
	//printf("slot data=%p.\n", get_slot_data(table, i));
	if (get_slot_data(table, i) != NULL) {
	    printf("table slot %d: %d (%p)\n", i, valueInt((Int)get_slot_data(table, i)), get_slot_data(table, i));
	}
    }
    //done with duplicate insert test.

    //printf("accessTable result=%s.\n", (accessTable(table, makeInt(915)) ? "true" : "false"));

    //printf("testLoc result=%d.\n", testLoc(table, makeInt(0)));

    /*
    // Testing Queues and TreeNodes.
    Queue myq = make_Queue(sizeof(TreeNode), copy_TreeNode, free_TreeNode);
    printf("made myq with sizeData=%lu bytes.\n", (long unsigned int)sizeof(TreeNode));
    TreeNode tn0 = make_TreeNode(1, 2, 3);
    printf("made tn0 = "); print_TreeNode(tn0); printf(".\n");
    TreeNode tn1 = make_TreeNode(4, 5, 6);
    printf("made tn1 = "); print_TreeNode(tn1); printf(".\n");
    TreeNode tn2 = make_TreeNode(7, 8, 9);
    printf("made tn2 = "); print_TreeNode(tn2); printf(".\n");
    enqueue(myq, tn0);
    enqueue(myq, tn1);
    enqueue(myq, tn2);
    printf("enqueued tnX.\n");
    printf(">Showing myq.\n");
    show_TreeNode_queue(myq);
    printf(">Done showing myq.\n");
    TreeNode tntemp = make_TreeNode(0, 0, 0);
    dequeue(myq, tntemp);
    printf("dequeued from myq.\n");
    printf(">Showing myq.\n");
    show_TreeNode_queue(myq);
    printf(">Done showing myq.\n");
    TreeNode tntemp2 = make_TreeNode(100, 200, 300);
    enqueue(myq, tntemp2);
    printf("enqueued tntemp2.\n");
    printf(">Showing myq.\n");
    show_TreeNode_queue(myq);
    printf(">Done showing myq.\n");
    TreeNode tntemp3 = make_TreeNode(99, 88, 77);
    enqueue(myq, tntemp3);
    printf("enqueued tntemp3.\n");
    printf(">Showing myq.\n");
    show_TreeNode_queue(myq);
    printf(">Done showing myq.\n");
    dequeue(myq, tntemp);
    printf("dequeued from myq.\n");
    printf(">Showing myq.\n");
    show_TreeNode_queue(myq);
    printf(">Done showing myq.\n");
    dequeue(myq, tntemp);
    printf("dequeued from myq.\n");
    printf(">Showing myq.\n");
    show_TreeNode_queue(myq);
    printf(">Done showing myq.\n");
    dequeue(myq, tntemp);
    printf("dequeued from myq.\n");
    printf(">Showing myq.\n");
    show_TreeNode_queue(myq);
    printf(">Done showing myq.\n");
    dequeue(myq, tntemp);
    printf("dequeued from myq.\n");
    printf(">Showing myq.\n");
    show_TreeNode_queue(myq);
    printf(">Done showing myq.\n");
    TreeNode tn3 = make_TreeNode(980, 870, 760);
    enqueue(myq, tn3);
    printf("enqueued tn3.\n");
    printf(">Showing myq.\n");
    show_TreeNode_queue(myq);
    printf(">Done showing myq.\n");
    free_TreeNode(tntemp);
    free_TreeNode(tntemp2);
    free_TreeNode(tntemp3);
    free_TreeNode(tn0);
    free_TreeNode(tn1);
    free_TreeNode(tn2);
    free_TreeNode(tn3);
    printf("freed tnX.\n");
    free_Queue(myq);
    printf("freed myq.\n");
    // Done testing Queues and TreeNodes.
    */

    //printT(table);

    freeTable(&table);
    //}

    return EXIT_SUCCESS;
}

/*
void show_TreeNode_queue(Queue q)
{
    ListIt li = listIterator(q);
    TreeNode tn = make_TreeNode(0, 0, 0);
    int i;
    for (i = 0; listNext(li, tn); i++) {
	printf("%d: ", i); print_TreeNode(tn); printf("\n");
    }
    printf("Queue length: %d.\n", lengthList(q));
    //FIXME: WTF? Uncommenting the below lines causes segfault; I'm ignoring it
    //for now.
    //free_TreeNode(tn);
    //free(li);
}
*/
