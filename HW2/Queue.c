/** @file
 * A queue implementation that extends List.c.
 */

#include <stdbool.h>
#include <stdlib.h>

#include "List.h"

typedef List Queue;
typedef void *DataQ;

/** Makes a queue.
 *
 * @param[in] sizeData the size of the queue's data objects
 * @param[in] copyData a function for copying data objects
 * @param[in] freeData a function for freeing data objects
 *
 * @return the new queue
 *
 * @see makeList()
 */
Queue make_Queue(size_t sizeData, void *(*copyData)(), void (*freeData)())
{
    return makeList(sizeData, copyData, freeData);
}

/** Frees a queue.
 *
 * @param[in,out] q the queue to free
 *
 * @see freeList()
 */
void free_Queue(Queue q)
{
    return freeList(q);
}

/** Enqueues a DataQ.
 *
 * @param[in] queue the queue to add a DataQ to
 * @param[in] node the node to enqueue. It is copied to the queue.
 *
 * @return false if a memory allocation fails; true otherwise
 */
bool enqueue(Queue queue, DataQ node)
{
    return insertTail(queue, node);
}

/** Dequeues a DataQ.
 *
 * @param[in] queue the queue to remove a DataQ from
 * @param[out] node the node to copy the dequeued DataQ data to
 *
 * @return false if queue or its head are NULL, or if the queue length is zero;
 * true otherwise
 */
bool dequeue(Queue queue, DataQ node)
{
    return deleteHead(queue, node);
}

/** Clears a queue. That is, all elements are freed and the resulting queue has
 * a size of zero.
 *
 * @param[in] q the queue to clear
 */
void clear_Queue(Queue q)
{
    return clearList(q);
}
