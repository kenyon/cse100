/* * * * * * * * * * * * * * * * * * * * * * * 
 *
 *	Table.h			spring 08
 *
 *	Walt Burkhard
 *
 * * * * * * * * * * * * * * * * * * * * * * */ 
#include <stdbool.h>
#include <stdlib.h>

typedef void *Table ;
typedef void *DataT ;

typedef struct { 
	// unsigned successful ; double unsuccessful ;
	unsigned successfulNumerator , successfulDenominator ;
	unsigned unsuccessfulNumerator , unsuccessfulDenominator ;
} Perform ;

Table 
makeTable ( size_t , size_t , int  (*diff)() ,
		unsigned (*hash)() , void * (*copy)() , void (*free)() ) ; 

void 
freeTable ( Table* ) ;

void
clearTable ( Table ) ;

bool
insertTable ( Table , DataT ) ;

bool 
accessTable ( Table , DataT ) ;

Perform
performTable ( Table ) ;

int
testLoc ( Table , void * ) ;

bool
testPB ( Table , unsigned int ) ;

/* Everything below was added by Kenyon. */
DataT get_slot_data(Table, unsigned int);
bool get_slot_isFull(Table, unsigned int);
bool get_slot_passbit(Table, unsigned int);
size_t get_tablesize(Table);
