/** @file
 * TreeNode for Table.
 *
 * @author Kenyon Ralph <kralph@ucsd.edu>
 * @date due 20080516
 */

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/** Tree nodes for use in Table.queue.
 */
typedef struct TreeNode {
    /** Table location.
     */
    unsigned int table_location;

    /** Step of the item moving into TreeNode.table_location.
     */
    unsigned int step;

    /** Location within the tree.
     */
    unsigned int tree_location;
} *TreeNode;

/** Makes a TreeNode.
 *
 * @param[in] table_location the table location
 * @param[in] step the step
 * @param[in] tree_location the tree location
 *
 * @return the newly created TreeNode
 */
TreeNode make_TreeNode(unsigned int table_location, unsigned int step, unsigned int tree_location)
{
    //printf(">Starting make_TreeNode.\n");

    TreeNode newtn = (TreeNode)malloc(sizeof(struct TreeNode));
    //printf("malloc'd %d bytes for newtn.\n", sizeof(struct TreeNode));

    newtn->table_location = table_location;
    //printf("set newtn->table_location=%d.\n", newtn->table_location);

    newtn->step = step;
    //printf("set newtn->step=%d.\n", newtn->step);

    newtn->tree_location = tree_location;
    //printf("set newtn->tree_location=%d.\n", newtn->tree_location);

    //printf(">Done with make_TreeNode.\n");
    return newtn;
}

/** TreeNode copy function.
 *
 * @param[out] dst TreeNode to copy to
 * @param[in] src TreeNode to copy from
 *
 * @return the original value of dst
 */
void *copy_TreeNode(TreeNode dst, TreeNode src)
{
    if (dst == NULL || src == NULL) return false;
    return memmove(dst, src, sizeof(struct TreeNode));
}

/** TreeNode free function.
 *
 * @param[in] tn the TreeNode to destroy
 */
void free_TreeNode(TreeNode tn)
{
    free(tn);
}

/** Prints a TreeNode to standard output like this:
 * (table_location, step, tree_location)
 *
 * @param[in] tn the TreeNode to print
 */
void print_TreeNode(TreeNode tn)
{
    printf("(%d, %d, %d)", tn->table_location, tn->step, tn->tree_location);
}

/** Sets a TreeNode's table location.
 *
 * @param tn the TreeNode to modify
 * @param loc the desired table location
 */
void set_table_location(TreeNode tn, unsigned int loc)
{
    tn->table_location = loc;
    return;
}

/** Sets a TreeNode's step.
 *
 * @param tn the TreeNode to modify
 * @param step the desired step
 */
void set_step(TreeNode tn, unsigned int step)
{
    tn->step = step;
    return;
}

/** Sets a TreeNode's tree location.
 *
 * @param tn the TreeNode to modify
 * @param loc the desired tree location
 */
void set_tree_location(TreeNode tn, unsigned int loc)
{
    tn->tree_location = loc;
    return;
}

/** Returns a TreeNode's table location.
 *
 * @param tn the TreeNode to look at
 *
 * @return the TreeNode.table_location.
 */
int get_table_location(TreeNode tn)
{
    return tn->table_location;
}

/** Returns a TreeNode's step.
 *
 * @param tn the TreeNode to look at
 *
 * @return the TreeNode.step.
 */
int get_step(TreeNode tn)
{
    return tn->step;
}

/** Returns a TreeNode's tree location.
 *
 * @param tn the TreeNode to look at
 *
 * @return the TreeNode.tree_location.
 */
int get_tree_location(TreeNode tn)
{
    return tn->tree_location;
}

/* vim: set si ai noet sts=1 tw=0 sw=4: */
